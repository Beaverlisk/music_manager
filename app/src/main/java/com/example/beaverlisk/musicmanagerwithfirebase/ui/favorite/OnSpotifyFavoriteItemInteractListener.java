package com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite;

import com.example.beaverlisk.musicmanagerwithfirebase.model.SpotifyEntity;

public interface OnSpotifyFavoriteItemInteractListener {

    void onFavoriteItemShareButtonClicked(SpotifyEntity spotifyEntity);

}
