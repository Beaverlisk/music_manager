package com.example.beaverlisk.musicmanagerwithfirebase.model;

import java.util.List;

public class ArtistData {

    private Artists artists;

    public Artists getArtists() {
        return artists;
    }

    public void setArtists(Artists artists) {
        this.artists = artists;
    }

    public static class Artists {

        private List<Artist> items = null;

        public List<Artist> getItems() {
            return items;
        }

        public void setItems(List<Artist> items) {
            this.items = items;
        }
    }
}

















