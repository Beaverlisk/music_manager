package com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification;

import android.app.PendingIntent;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public abstract class BaseNotification {

    private RemoteMessage remoteMessage;

    BaseNotification(RemoteMessage remoteMessage) {
        this.remoteMessage = remoteMessage;
    }

    @NonNull
    private String getTitleFromMessage() {
        Map<String, String> data = remoteMessage.getData();
        if (data.containsKey(NotificationConstants.KEY_MSG_TITLE)) {
            return data.get(NotificationConstants.KEY_MSG_TITLE);
        } else {
            return NotificationConstants.STRING_EMPTY;
        }
    }

    @NonNull
    private String getBodyFromMessage() {
        Map<String, String> data = remoteMessage.getData();
        if (data.containsKey(NotificationConstants.KEY_MSG_BODY)) {
            return data.get(NotificationConstants.KEY_MSG_BODY);
        } else {
            return NotificationConstants.STRING_EMPTY;
        }
    }

    @NonNull
    public String getTitle() {
        return getTitleFromMessage();
    }

    @NonNull
    public String getBody() {
        return getBodyFromMessage();
    }

    protected abstract PendingIntent configurePendingIntent(Context context);

    @DrawableRes
    protected abstract int getLargeIcon();

    @NonNull
    protected abstract String getNotificationTag();

    @NonNull
    protected abstract int getNotificationID();

    protected abstract String getChannelID();

}
