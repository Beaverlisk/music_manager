package com.example.beaverlisk.musicmanagerwithfirebase.ui.login;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.example.beaverlisk.musicmanagerwithfirebase.R;

public class ErrorDialogFragment extends DialogFragment {

    public static final String TAG = DialogFragment.class.getSimpleName()+"Tag";
    private static final String KEY_ERR_MSG = "ERR";

    public ErrorDialogFragment() {
    }

    public static ErrorDialogFragment newInstance(@Nullable String error) {
        ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_ERR_MSG, error);
        errorDialogFragment.setArguments(args);
        return errorDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.alert_dialog_error_title);
        String errorMessage = getArguments().getString(KEY_ERR_MSG);
        if (errorMessage == null) {
            builder.setMessage(R.string.alert_dialog_msg_unknown_error);
        } else {
            builder.setMessage(getArguments().getString(KEY_ERR_MSG));
        }
        builder.setPositiveButton(R.string.alert_dialog_btn_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

}

