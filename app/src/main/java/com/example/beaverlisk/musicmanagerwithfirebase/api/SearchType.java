package com.example.beaverlisk.musicmanagerwithfirebase.api;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.database.realm.AlbumRealmDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.database.realm.ArtistRealmDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.database.realm.TrackRealmDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Album;
import com.example.beaverlisk.musicmanagerwithfirebase.model.AlbumData;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Artist;
import com.example.beaverlisk.musicmanagerwithfirebase.model.ArtistData;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Track;
import com.example.beaverlisk.musicmanagerwithfirebase.model.TrackData;

import java.util.List;

import io.realm.RealmObject;
import retrofit2.Callback;

public enum SearchType {

    ARTIST("Artist", R.string.title_tab_artist, 0) {
        @Override
        public void saveResults(List<? extends RealmObject> data) {
            ArtistRealmDatabaseManager.getInstance().saveToDatabase((List<Artist>) data);
        }

        @Override
        public List<? extends RealmObject> getSavedResults(String query) {
            return ArtistRealmDatabaseManager.getInstance().qetRealmObjectsMatchingQuery(query);
        }

        @Override
        public <E> void getNetworkData(String query, Callback<E> dataCallback) {
            SpotifyRESTClient spotifyRESTClient = SpotifyAPI.getInstance().getSpotifyRESTClient();
            spotifyRESTClient.getArtistData(query, getSearchType()).enqueue((Callback<ArtistData>) dataCallback);
        }


        @Override
        public <T extends RealmObject, E> List<T> getResultFromBody(E body) {
            return (List<T>) ((ArtistData) body).getArtists().getItems();
        }
    },
    ALBUM("Album", R.string.title_tab_album, 1) {
        @Override
        public void saveResults(List<? extends RealmObject> data) {
            AlbumRealmDatabaseManager.getInstance().saveToDatabase((List<Album>) data);

        }

        @Override
        public List<? extends RealmObject> getSavedResults(String query) {
            return AlbumRealmDatabaseManager.getInstance().qetRealmObjectsMatchingQuery(query);
        }

        @Override
        public <E> void getNetworkData(String query, Callback<E> dataCallback) {
            SpotifyRESTClient spotifyRESTClient = SpotifyAPI.getInstance().getSpotifyRESTClient();
            spotifyRESTClient.getAlbumData(query, getSearchType()).enqueue((Callback<AlbumData>) dataCallback);
        }

        @Override
        public <T extends RealmObject, E> List<T> getResultFromBody(E body) {
            return (List<T>) ((AlbumData) body).getAlbums().getItems();
        }
    },
    TRACK("Track", R.string.title_tab_track, 2) {
        @Override
        public void saveResults(List<? extends RealmObject> data) {
            TrackRealmDatabaseManager.getInstance().saveToDatabase((List<Track>) data);
        }

        @Override
        public List<? extends RealmObject> getSavedResults(String query) {
            return TrackRealmDatabaseManager.getInstance().qetRealmObjectsMatchingQuery(query);
        }

        @Override
        public <E> void getNetworkData(String query, Callback<E> dataCallback) {
            SpotifyRESTClient spotifyRESTClient = SpotifyAPI.getInstance().getSpotifyRESTClient();
            spotifyRESTClient.getTrackData(query, getSearchType()).enqueue((Callback<TrackData>) dataCallback);
        }

        @Override
        public <T extends RealmObject, E> List<T> getResultFromBody(E body) {
            return (List<T>) ((TrackData) body).getTracks().getItems();
        }

    };

    private static SparseArray<SearchType> searchTypeArray = new SparseArray<>();

    static {
        for (SearchType searchType : SearchType.values()) {
            searchTypeArray.put(searchType.position, searchType);
        }
    }

    @NonNull
    private String searchType;
    private int pagerTabTitleResId;
    private int position;

    SearchType(@NonNull String searchType, int pagerTabTitleResId, int position) {
        this.searchType = searchType;
        this.pagerTabTitleResId = pagerTabTitleResId;
        this.position = position;
    }

    @NonNull
    public String getSearchType() {
        return searchType;
    }

    @NonNull
    public CharSequence getSearchTypePagerTabTitle(Context context) {
        return context.getString(pagerTabTitleResId);
    }

    @NonNull
    public static SearchType getSearchTypeForPagerPosition(int position) {
        return searchTypeArray.get(position);
    }

    public abstract List<? extends RealmObject> getSavedResults(String query);

    public abstract <T extends RealmObject, E> List<T> getResultFromBody(E body);

    public abstract <E> void getNetworkData(String query, Callback<E> dataCallback);

    public abstract void saveResults(List<? extends RealmObject> data);
}
