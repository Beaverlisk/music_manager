package com.example.beaverlisk.musicmanagerwithfirebase.ui.search.artist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Artist;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.DataProvider;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.OnSpotifyListItemListener;

import java.util.List;

public class ArtistFragment extends Fragment implements DataProvider {

    private ArtistAdapter adapter;
    private OnSpotifyListItemListener onSpotifyListItemListener;

    public ArtistFragment() {
    }

    @NonNull
    public static ArtistFragment newInstance() {
        return new ArtistFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onSpotifyListItemListener = (OnSpotifyListItemListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() + " must implement OnSpotifyListItemListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist, container, false);
        ListView listView = view.findViewById(R.id.list_view_artist_fragment);
        if (onSpotifyListItemListener != null) {
            adapter = new ArtistAdapter(onSpotifyListItemListener);
            listView.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListeningForValueEvent();
    }

    @Override
    public void setData(List<?> data) {
        if (data != null && data.size() > 0 && data.get(0) instanceof Artist) {
            if (adapter != null) {
                adapter.setData((List<Artist>) data);
            }
        }
    }

    @Override
    public void enableImageLoading(boolean enable) {
        adapter.enableImageLoading(enable);
    }

    @Override
    public void onStop() {
        adapter.stopListeningForValueEvent();
        super.onStop();
    }

    @Override
    public void onDetach() {
        onSpotifyListItemListener = null;
        super.onDetach();
    }

}
