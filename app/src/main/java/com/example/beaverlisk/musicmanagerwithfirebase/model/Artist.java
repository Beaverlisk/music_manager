package com.example.beaverlisk.musicmanagerwithfirebase.model;

import android.support.annotation.Nullable;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Artist extends RealmObject implements SpotifyEntity {

    @PrimaryKey
    private String id;
    private RealmList<String> genres = null;
    private RealmList<Image> images = null;
    private String name;
    private Integer popularity;
    private String type;
    private boolean isStarred;
    private String note;

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(RealmList<String> genres) {
        this.genres = genres;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(RealmList<Image> images) {
        this.images = images;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    @Override
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isStarred() {
        return isStarred;
    }

    public void setStarred(boolean starred) {
        isStarred = starred;
    }

    @Override
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Nullable
    public String getImageUrl() {
        String url = null;
        if (getImages() != null && getImages().size() > 0) {
            url = getImages().get(0).getUrl();
        }
        return url;
    }
}
