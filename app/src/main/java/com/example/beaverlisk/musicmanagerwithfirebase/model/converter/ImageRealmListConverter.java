package com.example.beaverlisk.musicmanagerwithfirebase.model.converter;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Image;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import io.realm.RealmList;

public class ImageRealmListConverter implements JsonSerializer<RealmList<Image>>, JsonDeserializer<RealmList<Image>> {

    @Override
    public JsonElement serialize(RealmList<Image> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (Image image : src) {
            jsonArray.add(context.serialize(image));
        }
        return jsonArray;
    }

    @Override
    public RealmList<Image> deserialize(JsonElement json, Type typeOfT,
                                        JsonDeserializationContext context) throws JsonParseException {
        RealmList<Image> images = new RealmList<>();
        JsonArray jsonArray = json.getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            images.add((Image) context.deserialize(jsonElement, Image.class));
        }
        return images;
    }
}

