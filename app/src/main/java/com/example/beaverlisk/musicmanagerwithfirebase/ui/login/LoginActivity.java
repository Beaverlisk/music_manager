package com.example.beaverlisk.musicmanagerwithfirebase.ui.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.beaverlisk.musicmanagerwithfirebase.BuildConfig;
import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.MainActivity;
import com.example.beaverlisk.musicmanagerwithfirebase.api.UserCredentialsHandler;
import com.example.beaverlisk.musicmanagerwithfirebase.utils.NetworkUtil;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String CLIENT_ID = "0cfeddb0cb37446695197e7721906b01";
    private static final String REDIRECT_URI = "music-manager-app://callback";
    private static final int REQUEST_CODE = 1337;

    private static final String KEY_IS_SPOTIFY_LOGIN_SCREEN_OPENED = "is_opened";
    private boolean isSpotifyLoginScreenOpened;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            isSpotifyLoginScreenOpened = savedInstanceState.getBoolean(KEY_IS_SPOTIFY_LOGIN_SCREEN_OPENED);
            if (isSpotifyLoginScreenOpened) {
                openSpotifyLoginWindow();
            }
        }
        setContentView(R.layout.activity_login);
        Button buttonLogin = findViewById(R.id.btn_login);
        buttonLogin.setOnClickListener(LoginActivity.this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (BuildConfig.DEBUG) {
                    showDebugMessage(getString(R.string.debug_msg_login_btn_pressed));
                }
                openSpotifyLoginWindow();
                isSpotifyLoginScreenOpened = true;
                break;
            default:
                break;
        }
    }

    private void openSpotifyLoginWindow() {
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
        builder.setScopes(new String[]{"streaming"});
        AuthenticationRequest request = builder.build();
        AuthenticationClient.openLoginActivity(LoginActivity.this, REQUEST_CODE, request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            isSpotifyLoginScreenOpened = false;
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);
            if (response != null) {
                switch (response.getType()) {
                    // Response was successful and contains auth token
                    case TOKEN:
                        UserCredentialsHandler.setToken(getApplicationContext(), response.getAccessToken(), response.getExpiresIn());
                        startMainActivity();
                        break;
                    // Auth flow returned an error
                    case ERROR:
                        if (!NetworkUtil.isNetworkConnected(LoginActivity.this)) {
                            showErrorDialog(getString(R.string.err_no_network_available));
                        } else {
                            showErrorDialog(response.getError());
                        }
                        break;
                    // Most likely auth flow was cancelled
                    default:
                        // Handle other cases
                        /* Spotify sdk does not handle user closing LoginActivity action(e.g. by pressing the back button).
                        *  See void onClientCancelled() in com.spotify.sdk.android.authentication.AuthenticationClient
                        */
                        if (response.getError() != null) {
                            showErrorDialog(response.getError());
                        }
                        break;
                }
            }
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    private void showErrorDialog(String error) {
        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(error);
        errorDialogFragment.show(getSupportFragmentManager(), ErrorDialogFragment.TAG);
    }

    private void showDebugMessage(String message) {
        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(KEY_IS_SPOTIFY_LOGIN_SCREEN_OPENED, isSpotifyLoginScreenOpened);
        super.onSaveInstanceState(outState);
    }
}
