package com.example.beaverlisk.musicmanagerwithfirebase.ui.search;

import java.util.List;

public interface DataProvider {

    void setData(List<?> data);

    void enableImageLoading(boolean enable);
}

