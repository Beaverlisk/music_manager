package com.example.beaverlisk.musicmanagerwithfirebase.ui.search;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.beaverlisk.musicmanagerwithfirebase.api.SearchType;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.album.AlbumFragment;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.artist.ArtistFragment;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.track.TrackFragment;

public class PagerSearchResultAdapter extends FragmentPagerAdapter {

    private Context context;

    PagerSearchResultAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        SearchType searchType = SearchType.getSearchTypeForPagerPosition(position);
        switch (searchType) {
            case ARTIST:
                return ArtistFragment.newInstance();
            case ALBUM:
                return AlbumFragment.newInstance();
            case TRACK:
                return TrackFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return SearchType.getSearchTypeForPagerPosition(position).getSearchTypePagerTabTitle(context);
    }

    @Override
    public int getCount() {
        return SearchType.values().length;
    }

}
