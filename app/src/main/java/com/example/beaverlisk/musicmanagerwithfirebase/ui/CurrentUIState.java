package com.example.beaverlisk.musicmanagerwithfirebase.ui;

import android.support.annotation.NonNull;

public enum CurrentUIState {

    SEARCH("Search"),
    FAVORITE("Favorite"),
    ADD_NOTE_DIALOG("AddNoteDialog");

    private String currentUIState;

    CurrentUIState(@NonNull String currentUIState) {
        this.currentUIState = currentUIState;
    }

    @NonNull
    public static CurrentUIState getCurrentState(String currentUIState) {
        for (CurrentUIState currentState : values()) {
            if (currentState.getCurrentStateString().equalsIgnoreCase(currentUIState)) {
                return currentState;
            }
        }
        return SEARCH;
    }

    @NonNull
    public String getCurrentStateString() {
        return currentUIState;
    }
}
