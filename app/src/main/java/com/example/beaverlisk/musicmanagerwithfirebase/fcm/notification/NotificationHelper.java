package com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.example.beaverlisk.musicmanagerwithfirebase.R;

public class NotificationHelper {

    private final Context context;
    private final NotificationManager notificationManager;

    public NotificationHelper(Context context) {
        this.context = context;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createNotificationChannel(String channelId) {
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(getNotificationChannel(channelId));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    private NotificationChannel getNotificationChannel(String channelId) {
        NotificationChannel notificationChannel = new NotificationChannel(channelId, getChannelName(channelId), NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.BLUE);
        notificationChannel.setSound(null, null);
        notificationChannel.enableVibration(true);
        notificationChannel.setVibrationPattern(null);
        return notificationChannel;
    }


    private Notification buildNotification(BaseNotification notification) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, notification.getChannelID());
        builder.setSmallIcon(getSmallIcon())
                .setAutoCancel(true)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), notification.getLargeIcon()))
                .setSmallIcon(getSmallIcon());
        builder.getExtras().putString(NotificationConstants.KEY_NOTIFICATION_TAG, notification.getNotificationTag());
        builder.setContentIntent(notification.configurePendingIntent(context));
        return builder.build();
    }


    public void showNotification(int notificationId, BaseNotification notification) {
        if (notificationManager != null) {
            notificationManager.notify(notificationId, buildNotification(notification));
        }
    }

    private int getSmallIcon() {
        return R.drawable.ic_logo_m;
    }

    @NonNull
    private String getChannelName(@NonNull String channelId) {
        switch (channelId) {
            case NotificationConstants.PRIMARY_CHANNEL_ID:
                return context.getString(NotificationConstants.PRIMARY_CHANNEL_NAME);
            default:
                throw new IllegalArgumentException("Unknown notification channel ID");
        }
    }

}
