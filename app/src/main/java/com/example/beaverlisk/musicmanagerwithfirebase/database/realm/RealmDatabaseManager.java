package com.example.beaverlisk.musicmanagerwithfirebase.database.realm;

import android.support.annotation.NonNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public abstract class RealmDatabaseManager<T extends RealmObject> {

    private Realm realm = null;
    private Class<T> realmObjectClass = null;

    RealmDatabaseManager(Class<T> realmObjectClass) {
        this.realmObjectClass = realmObjectClass;
    }

    private void openRealmInstance() {
        realm = Realm.getDefaultInstance();
    }

    private void closeRealmInstance() {
        if (realm == null || realm.isClosed()) {
            throw new IllegalStateException("Cannot close a Realm that is not open.");
        }
        realm.close();
    }

    protected void saveToDatabase(List<T> data) {
        openRealmInstance();
        try {
            if (data != null && data.size() > 0) {
                realm.beginTransaction();
                realm.insertOrUpdate(data);
                realm.commitTransaction();
            }
        } finally {
            closeRealmInstance();
        }
    }

    @NonNull
    protected List<T> qetRealmObjectsMatchingQuery(String query) {
        openRealmInstance();
        List<T> data = new ArrayList<>();
        Field[] realmObjectFields = realmObjectClass.getDeclaredFields();
        try {
            for (Field realmObjectField : realmObjectFields) {
                if (realmObjectField.getType().equals(String.class)) {
                    RealmResults<T> realmResults = realm.where(realmObjectClass).contains(realmObjectField.getName(), query, Case.INSENSITIVE).findAll();
                    data.addAll(realm.copyFromRealm(realmResults));
                }
            }
        } finally {
            closeRealmInstance();
        }
        return data;
    }

}
