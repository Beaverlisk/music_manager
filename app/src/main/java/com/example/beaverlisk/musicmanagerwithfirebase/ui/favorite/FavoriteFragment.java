package com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.database.FirebaseDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.model.SpotifyEntity;
import com.google.firebase.auth.FirebaseAuth;

public class FavoriteFragment extends Fragment {

    public static final String TAG = FavoriteFragment.class.getSimpleName() + "Tag";

    private ViewGroup progressBarLayout;

    private RecyclerView recyclerView;
    private OnSpotifyFavoriteItemInteractListener onSpotifyFavoriteItemInteractListener;

    public FavoriteFragment() {
    }

    @NonNull
    public static FavoriteFragment newInstance() {
        return new FavoriteFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onSpotifyFavoriteItemInteractListener = (OnSpotifyFavoriteItemInteractListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() + " must implement OnSpotifyFavoriteItemInteractListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        progressBarLayout = view.findViewById(R.id.layout_progress_bar);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        initToolbar(toolbar);
        recyclerView = view.findViewById(R.id.recycler_favorite);
        initRecycler(recyclerView);
        setRecyclerAdapter(recyclerView);
        setRecyclerSwipeBehaviour(recyclerView);
        TextView textViewCurrentUser = view.findViewById(R.id.tv_current_user);
        displayCurrentUserName(textViewCurrentUser);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (recyclerView != null) {
            FavoriteAdapter favoriteAdapter = (FavoriteAdapter) recyclerView.getAdapter();
            if (favoriteAdapter != null) {
                favoriteAdapter.startListeningForEvents();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    private void initToolbar(Toolbar toolbar) {
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.fragment_favorite_toolbar_title);
        /* Needed for sync with drawer toolbar */
        DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initRecycler(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
    }

    private void setRecyclerAdapter(RecyclerView recyclerView) {
        if (onSpotifyFavoriteItemInteractListener != null) {
            FavoriteAdapter adapter = new FavoriteAdapter(onSpotifyFavoriteItemInteractListener, progressBarLayout);
            recyclerView.setAdapter(adapter);
        }
    }

    private void setRecyclerSwipeBehaviour(final RecyclerView recyclerView) {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(
                0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                FavoriteAdapter adapter = (FavoriteAdapter) recyclerView.getAdapter();
                final int removingItemPosition = viewHolder.getAdapterPosition();
                final SpotifyEntity removingItem = adapter.getItemByPosition(removingItemPosition);
                adapter.removeItem(removingItemPosition);
                showSnackBar((View) recyclerView.getParent(), removingItem, removingItemPosition);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void displayCurrentUserName(TextView textViewCurrentUser) {
        String currentUser = getString(R.string.currently_logged_in_firebase,
                FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        textViewCurrentUser.setText(currentUser);
    }

    private void showSnackBar(View parentView, final SpotifyEntity removingItem, final int removingItemPosition) {
        Snackbar snackbar = Snackbar.make(parentView, R.string.snackbar_removed_from_favorite, Snackbar.LENGTH_LONG);
        final FavoriteAdapter adapter = (FavoriteAdapter) recyclerView.getAdapter();
        snackbar.addCallback(new Snackbar.Callback() {
            /* Favorite will be deleted from database only on snackbar timeout (user didn't press UNDO)*/
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                if (event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT) {
                    FirebaseDatabaseManager.getInstance().removeFromDatabase(removingItem);
                }
            }
        });
        snackbar.setAction(getString(R.string.snackbar_UNDO), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter != null) {
                    adapter.restoreItem(removingItemPosition, removingItem);
                }
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.colorAccent));
        snackbar.show();
    }

    @Override
    public void onStop() {
        if (recyclerView != null) {
            FavoriteAdapter favoriteAdapter = (FavoriteAdapter) recyclerView.getAdapter();
            if (favoriteAdapter != null) {
                favoriteAdapter.stopListeningForEvents();
            }
        }
        super.onStop();
    }

    @Override
    public void onDetach() {
        onSpotifyFavoriteItemInteractListener = null;
        super.onDetach();
    }
}

