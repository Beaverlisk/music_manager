package com.example.beaverlisk.musicmanagerwithfirebase.state;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

public class ConnectionStateRemoteService extends Service implements OnConnectionStateListener {

    public static final int MSG_IS_CHARGING = 101;
    public static final int MSG_IS_USB_CHARGING = 102;
    public static final int MSG_IS_AC_CHARGING = 103;
    public static final int MSG_IS_NETWORK_CONNECTED = 104;
    public static final int MSG_IS_WIFI_CONNECTED = 105;
    public static final int MSG_IS_MOBILE_DATA_CONNECTED = 106;

    public static final String KEY_IS_CHARGING = "isCharging";
    public static final String KEY_IS_USB_CHARGING = "isUsbCharging";
    public static final String KEY_IS_AC_CHARGING = "isAcCharging";
    public static final String KEY_IS_NETWORK_CONNECTED = "isNetworkConnected";
    public static final String KEY_IS_WIFI_CONNECTED = "isWiFiConnected";
    public static final String KEY_IS_MOBILE_DATA_CONNECTED = "isMobileDataConnected";

    private ConnectivityManager connectivityManager;

    private Boolean isCharging = null;
    private Boolean usbCharge = null;
    private Boolean acCharge = null;

    private BroadcastReceiver powerConnectionReceiver;
    private final Messenger messenger = new Messenger(new IncomingHandler(ConnectionStateRemoteService.this));

    @Override
    public void onCreate() {
        super.onCreate();
        connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        powerConnectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING;
                int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
                usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
                acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
            }
        };
        registerReceiver(powerConnectionReceiver, getPowerConnectionIntentFilter());
    }

    private IntentFilter getPowerConnectionIntentFilter() {
        return new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    private static class IncomingHandler extends Handler {
        private final WeakReference<ConnectionStateRemoteService> weakReferenceService;

        IncomingHandler(ConnectionStateRemoteService service) {
            weakReferenceService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            ConnectionStateRemoteService service = weakReferenceService.get();
            if (service != null) {
                Message responseMessage = Message.obtain(null, msg.what, 0, 0);
                Bundle responseExtras = new Bundle();
                switch (msg.what) {
                    case MSG_IS_CHARGING:
                        responseExtras.putSerializable(KEY_IS_CHARGING, service.isCharging());
                        break;
                    case MSG_IS_USB_CHARGING:
                        responseExtras.putSerializable(KEY_IS_USB_CHARGING, service.isUsbCharging());
                        break;
                    case MSG_IS_AC_CHARGING:
                        responseExtras.putSerializable(KEY_IS_AC_CHARGING, service.isAcCharging());
                        break;
                    case MSG_IS_NETWORK_CONNECTED:
                        responseExtras.putBoolean(KEY_IS_NETWORK_CONNECTED, service.isNetworkConnected());
                        break;
                    case MSG_IS_WIFI_CONNECTED:
                        responseExtras.putBoolean(KEY_IS_WIFI_CONNECTED, service.isWiFiConnected());
                        break;
                    case MSG_IS_MOBILE_DATA_CONNECTED:
                        responseExtras.putBoolean(KEY_IS_MOBILE_DATA_CONNECTED, service.isMobileDataConnected());
                        break;
                    default:
                        super.handleMessage(msg);
                }
                try {
                    responseMessage.setData(responseExtras);
                    msg.replyTo.send(responseMessage);
                } catch (RemoteException remoteException) {
                    remoteException.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        connectivityManager = null;
        if (powerConnectionReceiver != null) {
            unregisterReceiver(powerConnectionReceiver);
        }
    }

    @Override
    public Boolean isCharging() {
        return isCharging;
    }

    @Override
    public Boolean isAcCharging() {
        return acCharge;
    }

    @Override
    public Boolean isUsbCharging() {
        return usbCharge;
    }

    @Override
    public boolean isNetworkConnected() {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public boolean isWiFiConnected() {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting()
                && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

    @Override
    public boolean isMobileDataConnected() {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting()
                && activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
    }

}
