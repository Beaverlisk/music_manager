package com.example.beaverlisk.musicmanagerwithfirebase.database.realm;

import android.support.annotation.NonNull;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Artist;

import java.util.List;

public class ArtistRealmDatabaseManager extends RealmDatabaseManager<Artist> {

    private static ArtistRealmDatabaseManager instance = null;

    private ArtistRealmDatabaseManager() {
        super(Artist.class);
    }

    public static ArtistRealmDatabaseManager getInstance() {
        if (instance == null) {
            instance = new ArtistRealmDatabaseManager();
        }
        return instance;
    }

    public void saveToDatabase(List<Artist> data) {
        super.saveToDatabase(data);
    }

    @NonNull
    public List<Artist> qetRealmObjectsMatchingQuery(String query) {
        return super.qetRealmObjectsMatchingQuery(query);
    }
}

