package com.example.beaverlisk.musicmanagerwithfirebase.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.beaverlisk.musicmanagerwithfirebase.BuildConfig;
import com.example.beaverlisk.musicmanagerwithfirebase.MusicManagerApplication;
import com.jakewharton.disklrucache.DiskLruCache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.os.Environment.isExternalStorageRemovable;

class ImageDiskCache {

    private static final int MAX_SIZE = 1024 * 1024 * 10; // 10 MB
    private static final String DISK_CACHE_SUBDIR = "thumbnails";

    private DiskLruCache mDiskCache;

    ImageDiskCache() {
        File cacheDir = getCacheDir(MusicManagerApplication.getAppContext());
        try {
            mDiskCache = DiskLruCache.open(cacheDir, BuildConfig.VERSION_CODE, 1, MAX_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private File getCacheDir(Context context) {
        String cachePath = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ||
                !isExternalStorageRemovable() ? context.getExternalCacheDir().getPath() :
                context.getCacheDir().getPath();
        File cacheDir = new File(cachePath + File.separator + DISK_CACHE_SUBDIR);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        return cacheDir;
    }

    private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor) throws IOException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(editor.newOutputStream(0), 8 * 1024);
            return bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    void addBitmapToDiskCache(String key, Bitmap bitmap) {
       // checkNotOnMainThread();
        DiskLruCache.Editor editor = null;
        try {
            editor = mDiskCache.edit(key);
            if (editor == null) {
                return;
            }
            if (writeBitmapToFile(bitmap, editor)) {
                mDiskCache.flush();
                editor.commit();
            } else {
                editor.abort();
            }
        } catch (IOException e) {
            try {
                if (editor != null) {
                    editor.abort();
                }
            } catch (IOException ignored) {
            }
        }
    }

    @Nullable
    Bitmap getBitmapFromDiskCache(String key) {
       // checkNotOnMainThread();
        Bitmap bitmap = null;
        DiskLruCache.Snapshot snapshot = null;
        try {
            snapshot = mDiskCache.get(key);
            if (snapshot == null) {
                return null;
            }
            final InputStream in = snapshot.getInputStream(0);
            if (in != null) {
                final BufferedInputStream buffIn = new BufferedInputStream(in, 1024 * 8);
                bitmap = BitmapFactory.decodeStream(buffIn);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }
        return bitmap;
    }

    public boolean containsKey(String key) {
       // checkNotOnMainThread();
        boolean contained = false;
        DiskLruCache.Snapshot snapshot = null;
        try {
            snapshot = mDiskCache.get(key);
            contained = snapshot != null;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }
        return contained;
    }

    public void clearCache() {
        try {
            mDiskCache.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//
//    /**
//     * @throws IllegalStateException if the calling thread is the main/UI thread.
//     */
//    private static void checkNotOnMainThread() {
//        if (Looper.myLooper() == Looper.getMainLooper()) {
//            throw new IllegalStateException(
//                    "This method should not be called from the main/UI thread.");
//        }
//    }
}

