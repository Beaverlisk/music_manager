package com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.example.beaverlisk.musicmanagerwithfirebase.R;

public abstract class NotificationConstants {

    public static final String PRIMARY_CHANNEL_ID = "primary_channel";
    @StringRes
    static final int PRIMARY_CHANNEL_NAME = R.string.notification_primary_channel_name;

    static final String CONSOLE_NOTIFICATION_TYPE = "console";
    static final String CONSOLE_NOTIFICATION_TAG = ConsoleNotification.class.getSimpleName() + ".Tag";
    static final int CONSOLE_NOTIFICATION_ID = 101;
    @DrawableRes
    static final int CONSOLE_NOTIFICATION_LARGE_ICON = R.drawable.firebase_console_icon;

    static final String KEY_NOTIFICATION_TAG = "notification_tag";

    static final String KEY_MSG_TYPE = "type";
    static final String KEY_MSG_TITLE = "title";
    static final String KEY_MSG_BODY = "body";

    static final String STRING_EMPTY = "";

}
