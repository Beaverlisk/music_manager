package com.example.beaverlisk.musicmanagerwithfirebase.image;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.example.beaverlisk.musicmanagerwithfirebase.api.SpotifyAPI;
import com.example.beaverlisk.musicmanagerwithfirebase.api.SpotifyRESTClient;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageDownloader {

    private static ImageDownloader instance;
    private final ImageMemoryCache memoryCache;
    private final ImageDiskCache diskCache;

    public static ImageDownloader getInstance() {
        if (instance == null) {
            instance = new ImageDownloader();
        }
        return instance;
    }

    private ImageDownloader() {
        memoryCache = new ImageMemoryCache();
        diskCache = new ImageDiskCache();
    }

    public void downloadBitmap(@NonNull ImageView imageView, final String imageViewTag, @NonNull final String url) {
        String key = getKeyFromUrl(url);
        Bitmap bitmap = getBitmapFromCache(key);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            final WeakReference<ImageView> imageViewReference = new WeakReference<>(imageView);
            SpotifyRESTClient spotifyRESTClient = SpotifyAPI.getInstance().getSpotifyRESTClient();
            spotifyRESTClient
                    .getImage(url)
                    .enqueue(getCallback(imageViewReference, imageViewTag, url));
        }
    }

    /*
    *  TODO disk cache in separate thread
    *  Callbacks are executed on the application's main (UI) thread
    * */
    @NonNull
    private Callback<ResponseBody> getCallback(final WeakReference<ImageView> imageViewReference, @NonNull final String imageViewTag, final String url) {
        return new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (!response.isSuccessful() && response.body() == null) {
                    return;
                }

                ImageView imageView = null;
                if (imageViewReference != null) {
                    imageView = imageViewReference.get();
                }

                if (imageView != null) {
                    int imageViewHeight = imageView.getHeight();
                    int imageViewWidth = imageView.getWidth();
                    InputStream inputStream = null;
                    Bitmap bitmap;
                    try {
                        inputStream = response.body().byteStream();
                        bitmap = BitmapUtil.decodeSampledBitmapFromStream(inputStream, imageViewWidth, imageViewHeight);
                    } finally {
                        try {
                            if (inputStream != null) {
                                inputStream.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    String tag = (String) imageView.getTag();
                    if (bitmap != null) {
                        String key = getKeyFromUrl(url);
                        memoryCache.addBitmapToMemoryCache(key, bitmap);
                        diskCache.addBitmapToDiskCache(key, bitmap);
                        if (tag != null && tag.equals(imageViewTag)) {
                            imageView.setImageBitmap(memoryCache.getBitmapFromMemoryCache(key));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            }
        };
    }

    private String getKeyFromUrl(String url) {
        return url.substring(url.lastIndexOf('/') + 1).split("\\?")[0].split("#")[0];
    }

    @Nullable
    private Bitmap getBitmapFromCache(String key) {
        Bitmap bitmap = memoryCache.getBitmapFromMemoryCache(key);
        if (bitmap == null) {
            bitmap = diskCache.getBitmapFromDiskCache(key);
        }
        return bitmap;
    }

}

