package com.example.beaverlisk.musicmanagerwithfirebase.ui.search.album;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Album;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.DataProvider;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.OnSpotifyListItemListener;

import java.util.List;


public class AlbumFragment extends Fragment implements DataProvider {

    private AlbumAdapter adapter;
    private OnSpotifyListItemListener onSpotifyListItemListener;

    public AlbumFragment() {
    }

    @NonNull
    public static AlbumFragment newInstance() {
        return new AlbumFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onSpotifyListItemListener = (OnSpotifyListItemListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() + " must implement OnSpotifyListItemListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_album, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        initRecycler(recyclerView);
        setRecyclerAdapter(recyclerView);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListeningForValueEvent();
    }

    private void initRecycler(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.addItemDecoration(getDividerItemDecorationForRecycler());
    }

    @NonNull
    private DividerItemDecoration getDividerItemDecorationForRecycler() {
        return new DividerItemDecoration(getContext(), new LinearLayoutManager(getContext()).getOrientation());
    }

    public void setRecyclerAdapter(RecyclerView recyclerView) {
        if (onSpotifyListItemListener != null) {
            adapter = new AlbumAdapter(onSpotifyListItemListener);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void setData(List<?> data) {
        if (data != null && data.size() > 0 && data.get(0) instanceof Album) {
            if (adapter != null) {
                adapter.setData((List<Album>) data);
            }
        }
    }

    @Override
    public void enableImageLoading(boolean enable) {
        adapter.enableImageLoading(enable);
    }

    @Override
    public void onStop() {
        adapter.stopListeningForValueEvent();
        super.onStop();
    }

    @Override
    public void onDetach() {
        onSpotifyListItemListener = null;
        super.onDetach();

    }
}

