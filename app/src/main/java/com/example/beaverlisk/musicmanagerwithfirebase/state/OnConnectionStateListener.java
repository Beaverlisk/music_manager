package com.example.beaverlisk.musicmanagerwithfirebase.state;

import android.support.annotation.Nullable;

public interface OnConnectionStateListener {

    @Nullable
    Boolean isCharging();

    @Nullable
    Boolean isUsbCharging();

    @Nullable
    Boolean isAcCharging();

    boolean isNetworkConnected();

    boolean isWiFiConnected();

    boolean isMobileDataConnected();

}
