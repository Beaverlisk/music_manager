package com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.example.beaverlisk.musicmanagerwithfirebase.R;

public class NoteDialogFragment extends DialogFragment {

    public static final String TAG = DialogFragment.class.getSimpleName() + "Tag";
    private OnAddNoteListener onAddNoteListener;

    public interface OnAddNoteListener {
        void onDialogClick(@Nullable String note);
    }

    public NoteDialogFragment() {
    }

    @NonNull
    public static NoteDialogFragment newInstance() {
        return new NoteDialogFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onAddNoteListener = (OnAddNoteListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() + " must implement OnAddNoteListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_note, null);
        builder.setView(view)
                .setMessage(R.string.dialog_note_title)
                .setPositiveButton(R.string.dialog_note_ok_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText noteEditText = view.findViewById(R.id.edit_text_note);
                        if (onAddNoteListener != null) {
                            onAddNoteListener.onDialogClick(noteEditText.getText().toString());
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_note_cancel_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (onAddNoteListener != null) {
                            onAddNoteListener.onDialogClick(null);
                        }
                    }
                });
        return builder.create();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onAddNoteListener = null;
    }

}
