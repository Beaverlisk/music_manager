package com.example.beaverlisk.musicmanagerwithfirebase.model;

public class Favorite implements SpotifyEntity {

    private String id;
    private String name;
    private String type;
    private String note;

    public Favorite() {
    }

    public Favorite(String id, String name, String type, String note) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.note = note;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
