package com.example.beaverlisk.musicmanagerwithfirebase.model.converter;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Track;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import io.realm.RealmList;

public class TrackRealmListConverter implements JsonSerializer<RealmList<Track>>, JsonDeserializer<RealmList<Track>> {

    @Override
    public JsonElement serialize(RealmList<Track> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (Track track : src) {
            jsonArray.add(context.serialize(track));
        }
        return jsonArray;
    }

    @Override
    public RealmList<Track> deserialize(JsonElement json, Type typeOfT,
                                        JsonDeserializationContext context) throws JsonParseException {
        RealmList<Track> tracks = new RealmList<>();
        JsonArray jsonArray = json.getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            tracks.add((Track) context.deserialize(jsonElement, Track.class));
        }
        return tracks;
    }
}
