package com.example.beaverlisk.musicmanagerwithfirebase.database.realm;


import android.support.annotation.NonNull;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Track;

import java.util.List;

public class TrackRealmDatabaseManager extends RealmDatabaseManager<Track> {

    private static TrackRealmDatabaseManager instance = null;

    private TrackRealmDatabaseManager() {
        super(Track.class);
    }

    public static TrackRealmDatabaseManager getInstance() {
        if (instance == null) {
            instance = new TrackRealmDatabaseManager();
        }
        return instance;
    }

    public void saveToDatabase(List<Track> data) {
        super.saveToDatabase(data);
    }

    @NonNull
    public List<Track> qetRealmObjectsMatchingQuery(String query) {
        return super.qetRealmObjectsMatchingQuery(query);
    }
}


