package com.example.beaverlisk.musicmanagerwithfirebase.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.concurrent.TimeUnit;

public class UserCredentialsHandler {

    private static final String PREF_NAME = "musicmanager_token";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String EXPIRES_AT = "expires_at";

    public static void setToken(@NonNull Context appContext, @Nullable String token, int expiresIn) {
        long expiresAt = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(expiresIn);
        SharedPreferences sharedPreferences = getSharedPreferences(appContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_TOKEN, token);
        editor.putLong(EXPIRES_AT, expiresAt);
        editor.apply();
    }

    @Nullable
    public static String getToken(@NonNull Context appContext) {
        SharedPreferences sharedPref = getSharedPreferences(appContext);
        String token = sharedPref.getString(ACCESS_TOKEN, null);
        long expiresAt = sharedPref.getLong(EXPIRES_AT, 0);
        if (token == null || expiresAt < System.currentTimeMillis()) {
            return null;
        }
        return token;
    }

    private static SharedPreferences getSharedPreferences(@NonNull Context appContext) {
        return appContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

}
