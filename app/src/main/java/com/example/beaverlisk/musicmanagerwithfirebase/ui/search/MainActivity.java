package com.example.beaverlisk.musicmanagerwithfirebase.ui.search;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.api.SearchType;
import com.example.beaverlisk.musicmanagerwithfirebase.database.FirebaseDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.model.SpotifyEntity;
import com.example.beaverlisk.musicmanagerwithfirebase.state.ConnectionStateRemoteService;
import com.example.beaverlisk.musicmanagerwithfirebase.state.OnConnectionStateListener;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.CurrentUIState;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite.FavoriteFragment;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite.NoteDialogFragment;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite.OnSpotifyFavoriteItemInteractListener;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite.firebase.FirebaseAuthFragment;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite.firebase.FirebaseAuthListener;
import com.example.beaverlisk.musicmanagerwithfirebase.user.User;
import com.example.beaverlisk.musicmanagerwithfirebase.utils.NetworkUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        OnSpotifyListItemListener, NoteDialogFragment.OnAddNoteListener,
        OnSpotifyFavoriteItemInteractListener, OnConnectionStateListener,
        FirebaseAuthListener {

    private static final String KEY_CURRENT_STATE = "current_state";
    private static final String KEY_SELECTED_SPOTIFY_ENTITY = "selected_spotify_entity";
    private CurrentUIState currentState;

    private Boolean isCharging = null;
    private Boolean isUSBCharging = null;
    private Boolean isAcCharging = null;
    private boolean isNetworkConnected = false;
    private boolean isWiFiConnected = false;
    private boolean isMobileDataConnected = false;

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private SpotifyEntity starredSpotifyEntity;

    private boolean isConnectionStateRemoteServiceBound = false;
    private Messenger connectionStateRemoteService;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            connectionStateRemoteService = new Messenger(service);
            isConnectionStateRemoteServiceBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            connectionStateRemoteService = null;
            isConnectionStateRemoteServiceBound = false;
        }
    };

    private static class ResponseHandler extends Handler {
        private final WeakReference<MainActivity> weakReferenceActivity;

        private ResponseHandler(MainActivity activity) {
            weakReferenceActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = weakReferenceActivity.get();
            if (activity != null) {
                Bundle msgExtra = msg.getData();
                switch (msg.what) {
                    case ConnectionStateRemoteService.MSG_IS_CHARGING:
                        activity.isCharging = (Boolean) msgExtra.getSerializable(ConnectionStateRemoteService.KEY_IS_CHARGING);
                        break;
                    case ConnectionStateRemoteService.MSG_IS_USB_CHARGING:
                        activity.isUSBCharging = (Boolean) msgExtra.getSerializable(ConnectionStateRemoteService.KEY_IS_USB_CHARGING);
                        break;
                    case ConnectionStateRemoteService.MSG_IS_AC_CHARGING:
                        activity.isAcCharging = (Boolean) msgExtra.getSerializable(ConnectionStateRemoteService.KEY_IS_AC_CHARGING);
                        break;
                    case ConnectionStateRemoteService.MSG_IS_NETWORK_CONNECTED:
                        activity.isNetworkConnected = msgExtra.getBoolean(ConnectionStateRemoteService.KEY_IS_NETWORK_CONNECTED);
                        break;
                    case ConnectionStateRemoteService.MSG_IS_WIFI_CONNECTED:
                        activity.isWiFiConnected = msgExtra.getBoolean(ConnectionStateRemoteService.KEY_IS_WIFI_CONNECTED);
                        break;
                    case ConnectionStateRemoteService.MSG_IS_MOBILE_DATA_CONNECTED:
                        activity.isMobileDataConnected = msgExtra.getBoolean(ConnectionStateRemoteService.KEY_IS_MOBILE_DATA_CONNECTED);
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CurrentUIState restoredState = CurrentUIState.SEARCH; // default state
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_CURRENT_STATE)) {
            restoredState = CurrentUIState.getCurrentState(savedInstanceState.getString(KEY_CURRENT_STATE,
                    CurrentUIState.SEARCH.getCurrentStateString()));
        }
        initDrawer(restoredState);
        if (restoredState == CurrentUIState.SEARCH) {
            showSearchFragment();
        } else if (restoredState == CurrentUIState.FAVORITE) {
            openFirebaseFragment(false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent serviceIntent = new Intent(MainActivity.this, ConnectionStateRemoteService.class);
        isConnectionStateRemoteServiceBound = bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void initDrawer(CurrentUIState restoredState) {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        toggleNavigationViewMenu(restoredState);
    }

    @Override
    public void onItemFavoriteButtonClicked(@NonNull SpotifyEntity spotifyEntity, boolean addToFavorite) {
        if (FirebaseDatabaseManager.getInstance().getCurrentUser() != null) {
            if (addToFavorite) {
                this.starredSpotifyEntity = spotifyEntity;
                NoteDialogFragment.newInstance().show(getSupportFragmentManager(), NoteDialogFragment.TAG);
                currentState = CurrentUIState.ADD_NOTE_DIALOG;
            } else {
                FirebaseDatabaseManager.getInstance().removeFromDatabase(spotifyEntity);
            }
        } else {
            Toast.makeText(this, R.string.msg_log_in_to_firebase, Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Saving favorites to database occurs after clicking button (positive or negative) in note dialog.
     * Adding a note is optional, but dialog with edit text is shown every time user click star button
     */
    @Override
    public void onDialogClick(@Nullable String note) {
        if (starredSpotifyEntity != null) {
            FirebaseDatabaseManager.getInstance().saveToDatabase(starredSpotifyEntity);
            starredSpotifyEntity = null;
            currentState = CurrentUIState.SEARCH;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.drawer_search:
                if (currentState != CurrentUIState.SEARCH) {
                    showSearchFragment();
                }
                break;
            case R.id.drawer_favorite:
                if (currentState != CurrentUIState.FAVORITE) {
                    openFirebaseFragment(true);
                }
                break;
            case R.id.drawer_settings:
                break;
            default:
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFavoriteItemShareButtonClicked(SpotifyEntity spotifyEntity) {
        String extraString = generateExtraString(spotifyEntity);
        if (extraString != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, extraString);
            shareIntent.setType("text/plain");
            if (shareIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(Intent.createChooser(shareIntent, getString(R.string.favorite_share_chooser_title)));
            }
        }
    }

    @Nullable
    private String generateExtraString(@NonNull SpotifyEntity spotifyEntity) {
        String extraString = null;
        String type = spotifyEntity.getType();
        if (type.equalsIgnoreCase(SearchType.ARTIST.getSearchType())) {
            extraString = getString(R.string.favorite_share_artist, spotifyEntity.getName());
        } else if (type.equalsIgnoreCase(SearchType.ALBUM.getSearchType())) {
            extraString = getString(R.string.favorite_share_album, spotifyEntity.getName());
        } else if (type.equalsIgnoreCase(SearchType.TRACK.getSearchType())) {
            extraString = getString(R.string.favorite_share_track, spotifyEntity.getName());
        }
        return extraString;
    }

    private void sendMessageToConnectionStateRemoteService(int msgKey) {
        if (isConnectionStateRemoteServiceBound) {
            try {
                Message msg = Message.obtain(null, msgKey, 0, 0);
                msg.replyTo = new Messenger(new ResponseHandler(MainActivity.this));
                connectionStateRemoteService.send(msg);
            } catch (RemoteException remoteException) {
                remoteException.printStackTrace();
            }
        }
    }

    private void showSearchFragment() {
        currentState = CurrentUIState.SEARCH;
        clearFragmentBackStack();
        SearchFragment searchFragment = (SearchFragment) getSupportFragmentManager()
                .findFragmentByTag(SearchFragment.TAG);
        if (searchFragment == null) {
            searchFragment = SearchFragment.newInstance();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, searchFragment, SearchFragment.TAG)
                .commit();
    }

    private void openFirebaseFragment(boolean addToBackStack) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            showFirebaseAuthFragment(addToBackStack);
        } else {
            showFavoriteFragment(addToBackStack);
        }
    }

    private void showFavoriteFragment(boolean addToBackStack) {
        currentState = CurrentUIState.FAVORITE;
        FavoriteFragment favoriteFragment = (FavoriteFragment) getSupportFragmentManager()
                .findFragmentByTag(FavoriteFragment.TAG);
        if (favoriteFragment == null) {
            favoriteFragment = FavoriteFragment.newInstance();
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.fragment_container, favoriteFragment, FavoriteFragment.TAG).commit();
    }

    private void showFirebaseAuthFragment(boolean addToBackStack) {
        currentState = CurrentUIState.FAVORITE;
        FirebaseAuthFragment firebaseAuthFragment = (FirebaseAuthFragment) getSupportFragmentManager()
                .findFragmentByTag(FirebaseAuthFragment.TAG);
        if (firebaseAuthFragment == null) {
            firebaseAuthFragment = FirebaseAuthFragment.newInstance();
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(R.id.fragment_container, firebaseAuthFragment, FirebaseAuthFragment.TAG).commit();
    }

    private void clearFragmentBackStack() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    /* TODO:
     * Sure that a better solution for drawer state synchronization (toggling) exists.
     * I'll return here a little bit later
     */
    private void toggleNavigationViewMenu(CurrentUIState toState) {
        if (navigationView != null) {
            if (toState == CurrentUIState.SEARCH) {
                if (currentState == CurrentUIState.SEARCH) {
                    navigationView.getMenu().getItem(1).setChecked(true);
                } else if (currentState == CurrentUIState.FAVORITE) {
                    navigationView.getMenu().getItem(0).setChecked(true);
                } else {
                    navigationView.getMenu().getItem(0).setChecked(true);
                }
            } else {
                if (currentState == CurrentUIState.SEARCH) {
                    navigationView.getMenu().getItem(0).setChecked(true);
                } else if (currentState == CurrentUIState.FAVORITE) {
                    navigationView.getMenu().getItem(1).setChecked(true);
                }
            }
        }
    }

    @Nullable
    @Override
    public Boolean isCharging() {
        sendMessageToConnectionStateRemoteService(ConnectionStateRemoteService.MSG_IS_CHARGING);
        return isCharging;
    }

    @Nullable
    @Override
    public Boolean isUsbCharging() {
        sendMessageToConnectionStateRemoteService(ConnectionStateRemoteService.MSG_IS_USB_CHARGING);
        return isUSBCharging;
    }

    @Nullable
    @Override
    public Boolean isAcCharging() {
        sendMessageToConnectionStateRemoteService(ConnectionStateRemoteService.MSG_IS_AC_CHARGING);
        return isAcCharging;
    }

    @Override
    public boolean isNetworkConnected() {
        if (isConnectionStateRemoteServiceBound) {
            sendMessageToConnectionStateRemoteService(ConnectionStateRemoteService.MSG_IS_NETWORK_CONNECTED);
            return isNetworkConnected;
        } else {
            return NetworkUtil.isNetworkConnected(getApplicationContext());
        }
    }

    @Override
    public boolean isWiFiConnected() {
        if (isConnectionStateRemoteServiceBound) {
            sendMessageToConnectionStateRemoteService(ConnectionStateRemoteService.MSG_IS_WIFI_CONNECTED);
            return isWiFiConnected;
        } else {
            return NetworkUtil.isWifiConnected(getApplicationContext());
        }
    }

    @Override
    public boolean isMobileDataConnected() {
        if (isConnectionStateRemoteServiceBound) {
            sendMessageToConnectionStateRemoteService(ConnectionStateRemoteService.MSG_IS_MOBILE_DATA_CONNECTED);
            return isMobileDataConnected;
        } else {
            return NetworkUtil.isMobileDataConnected(getApplicationContext());
        }
    }

    @Override
    public void onAuthSuccess() {
        Toast.makeText(this, R.string.msg_authentication_successful, Toast.LENGTH_SHORT).show();
        FirebaseDatabaseManager.getInstance().saveCurrentUserToDatabase();
        registerUserForCrashlytics();
        showFavoriteFragment(false);
    }

    @Override
    public void onAuthFailed() {
        Toast.makeText(this, R.string.msg_authentication_failed, Toast.LENGTH_SHORT).show();
    }

    private void registerUserForCrashlytics() {
        FirebaseUser currentUser = FirebaseDatabaseManager.getInstance().getCurrentUser();
        if (currentUser != null) {
            Crashlytics.setUserIdentifier(currentUser.getUid());
            Crashlytics.setUserEmail(currentUser.getDisplayName());
            Crashlytics.setUserName(currentUser.getEmail());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (currentState != null) {
            outState.putString(KEY_CURRENT_STATE, currentState.getCurrentStateString());
            if (currentState == CurrentUIState.ADD_NOTE_DIALOG && starredSpotifyEntity != null) {
                // TODO: return here
                // need to handle NoteDialog behaviour on config change and save starredSpotifyEntity for further use
            }
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            clearFragmentBackStack();
            toggleNavigationViewMenu(CurrentUIState.SEARCH);
            currentState = CurrentUIState.SEARCH;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isConnectionStateRemoteServiceBound) {
            unbindService(serviceConnection);
            isConnectionStateRemoteServiceBound = false;
        }
    }

}
