package com.example.beaverlisk.musicmanagerwithfirebase.api;

import android.support.annotation.NonNull;

import com.example.beaverlisk.musicmanagerwithfirebase.MusicManagerApplication;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Album;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Artist;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Image;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Track;
import com.example.beaverlisk.musicmanagerwithfirebase.model.converter.AlbumRealmListConverter;
import com.example.beaverlisk.musicmanagerwithfirebase.model.converter.ArtistRealmListConverter;
import com.example.beaverlisk.musicmanagerwithfirebase.model.converter.ImageRealmListConverter;
import com.example.beaverlisk.musicmanagerwithfirebase.model.converter.TrackRealmListConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import io.realm.RealmList;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SpotifyAPI {

    private static final String BASE_URL = "https://api.spotify.com/";
    private static final String HEADER_AUTH_KEY = "Authorization";
    private static final String BEARER = "Bearer ";

    private static SpotifyAPI instance;
    private SpotifyRESTClient spotifyRESTClient;

    private SpotifyAPI() {
        buildRetrofit();
    }

    public static SpotifyAPI getInstance() {
        if (instance == null) {
            instance = new SpotifyAPI();
        }
        return instance;
    }

    private void buildRetrofit() {
        OkHttpClient client = getHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(buildConfiguredGson()))
                .client(client)
                .build();
        spotifyRESTClient = retrofit.create(SpotifyRESTClient.class);
    }

    /*
     * Interceptor for auth header to be passed automatically with every request
     */
    @NonNull
    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                String token = BEARER + UserCredentialsHandler.getToken(MusicManagerApplication.getAppContext());
                Request request = original.newBuilder()
                        .header(HEADER_AUTH_KEY, token)
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });
        return httpClient.build();
    }

    @NonNull
    private Gson buildConfiguredGson() {
        return new GsonBuilder()
                .registerTypeAdapter(new TypeToken<RealmList<Album>>() {
                        }.getType(),
                        new AlbumRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<Artist>>() {
                        }.getType(),
                        new ArtistRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<Track>>() {
                        }.getType(),
                        new TrackRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<Image>>() {
                        }.getType(),
                        new ImageRealmListConverter())
                .create();
    }

    public SpotifyRESTClient getSpotifyRESTClient() {
        return spotifyRESTClient;
    }
}
