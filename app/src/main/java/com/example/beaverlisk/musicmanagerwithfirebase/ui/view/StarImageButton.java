package com.example.beaverlisk.musicmanagerwithfirebase.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.example.beaverlisk.musicmanagerwithfirebase.R;

public class StarImageButton extends AppCompatImageButton {

    private static final int DEFAULT_SIZE = 46;
    private static final int DEFAULT_PADDING = 6;
    private static final int DEFAULT_ANIMATION_DURATION = 150;
    private static final boolean DRAW_CIRCLE = false;
    private static final int STAR = R.drawable.ic_star_gold_24_dp;
    private static final int STAR_BORDER = R.drawable.ic_star_border_grey_24_dp;

    private OnClickListener customizedClickListener;

    private int btnSize;
    private int btnPadding;
    private int animationDuration;
    private boolean isDrawCircle;

    private boolean isStarred;
    private Paint paint;

    public StarImageButton(Context context) {
        super(context);
        init(context, null);
    }

    public StarImageButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public StarImageButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        btnSize = convertDpToPx(DEFAULT_SIZE);
        btnPadding = convertDpToPx(DEFAULT_PADDING);
        animationDuration = DEFAULT_ANIMATION_DURATION;
        isDrawCircle = DRAW_CIRCLE;
        if (!isInEditMode()) {
            if (attrs != null) {
                initAttributes(context, attrs);
            }
            if (isStarred) {
                setImageResource(STAR);
            } else {
                setImageResource(STAR_BORDER);
            }
            setPadding(btnPadding, btnPadding, btnPadding, btnPadding);
            /*
             * StarImageButton  does not allow to set its background - it's not supposed to
             * Src image was set as imageResource (not backgroundResource) for possibility
             * of changing button size avoiding blurring of src image
             */
            setBackgroundDrawable(null);
            initPaint();
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleStarState(!isStarred);
                    if (customizedClickListener != null)
                        customizedClickListener.onClick(v);
                }
            });
        }
    }

    private void initAttributes(Context context, AttributeSet attributeSet) {
        TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.StarImageButton);
        if (attr != null) {
            try {
                btnSize = convertDpToPx(attr.getInt(R.styleable.StarImageButton_button_size, DEFAULT_SIZE));
                btnPadding = convertDpToPx(attr.getInt(R.styleable.StarImageButton_button_padding, DEFAULT_PADDING));
                animationDuration = attr.getInt(R.styleable.StarImageButton_animation_duration, DEFAULT_ANIMATION_DURATION);
                isDrawCircle = attr.getBoolean(R.styleable.StarImageButton_draw_circle, DRAW_CIRCLE);
            } finally {
                attr.recycle();
            }
        }
    }

    private void initPaint() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.GRAY);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE);
    }

    public void toggleStarState(boolean starred) {
        if (isStarred != starred) {
            isStarred = starred;
            updateStarState(starred);
        }
    }

    private void updateStarState(boolean starred) {
        if (starred) {
            animateButton(starred);
            super.setImageResource(STAR);
        } else {
            animateButton(starred);
            super.setImageResource(STAR_BORDER);
        }
    }

    private void animateButton(boolean toStared) {
        float startBounce;
        float endBounce;
        if (toStared) {
            startBounce = 0.2f;
            endBounce = 1.0f;
        } else {
            startBounce = 1.3f;
            endBounce = 1.0f;
        }

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator bounceX = ObjectAnimator.ofFloat(this, "scaleX", startBounce, endBounce);
        bounceX.setDuration(animationDuration);
        bounceX.setInterpolator(new AccelerateInterpolator());

        ObjectAnimator bounceY = ObjectAnimator.ofFloat(this, "scaleY", startBounce, endBounce);
        bounceY.setDuration(animationDuration);
        bounceY.setInterpolator(new AccelerateInterpolator());
        bounceY.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (isStarred) {
                    setImageResource(STAR);
                } else {
                    setImageResource(STAR_BORDER);
                }
            }
        });

        animatorSet.play(bounceX).with(bounceY);
        animatorSet.start();
    }

    /*
     * Allows to draw a circle around star image using canvas.
     * Added for educational purposes, can be enabled by setting flag "draw_circle = true" in xml
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isDrawCircle) {
            float cx = getWidth() / 2;
            float cy = getHeight() / 2;
            float radius = getWidth() / 4;
            canvas.drawCircle(cx, cy, radius, paint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(btnSize, btnSize);
    }

    private int convertDpToPx(int dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
        return (int) px;
    }

    public boolean isStarred() {
        return isStarred;
    }

    /* OnClickListener wrapper to avoid replacement when setOnClickListener invokes */
    public void addOnClickListener(OnClickListener onClickListener) {
        this.customizedClickListener = onClickListener;
    }
}