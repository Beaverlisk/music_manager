package com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class NotificationCreator {

    private final NotificationHelper notificationHelper;

    public NotificationCreator(Context context) {
        notificationHelper = new NotificationHelper(context);
    }

    public void showNotification(RemoteMessage remoteMessage) {
        String notificationType = getNotificationType(remoteMessage);
        BaseNotification notification = createTypedNotification(notificationType, remoteMessage);
        if (notification != null) {
            notificationHelper.showNotification(notification.getNotificationID(), notification);
        }
    }

    @NonNull
    private String getNotificationType(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        if (data.containsKey(NotificationConstants.KEY_MSG_TYPE)) {
            return data.get(NotificationConstants.KEY_MSG_TYPE);
        }
        return NotificationConstants.STRING_EMPTY;
    }

    @Nullable
    private BaseNotification createTypedNotification(String messageType, RemoteMessage remoteMessage) {
        switch (messageType) {
            case NotificationConstants.CONSOLE_NOTIFICATION_TYPE:
                return new ConsoleNotification(remoteMessage);
            default:
            case NotificationConstants.STRING_EMPTY:
                return new ConsoleNotification(remoteMessage);
        }
    }

}
