package com.example.beaverlisk.musicmanagerwithfirebase.ui.search;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.api.RetrofitCallback;
import com.example.beaverlisk.musicmanagerwithfirebase.api.SearchType;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Artist;
import com.example.beaverlisk.musicmanagerwithfirebase.model.ArtistData;
import com.example.beaverlisk.musicmanagerwithfirebase.state.OnConnectionStateListener;

import java.util.List;

import io.realm.RealmObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    public static final String TAG = SearchFragment.class.getSimpleName() + "Tag";
    private static final String KEY_PAGER_POSITION = "saved_pager_position";
    private static final String KEY_SEARCH_VIEW_QUERY = "saved_search_query";

    private OnConnectionStateListener onConnectionStateListener;
    private String predefinedSearchViewQuery = null;

    private ViewPager viewPager;
    private SearchView searchView;

    public SearchFragment() {
    }

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onConnectionStateListener = (OnConnectionStateListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() + " must implement OnConnectionStateListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onConnectionStateListener = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int predefinedPosition = 0;
        if (savedInstanceState != null) {
            predefinedPosition = savedInstanceState.getInt(KEY_PAGER_POSITION, 0);
            predefinedSearchViewQuery = savedInstanceState.getString(KEY_SEARCH_VIEW_QUERY, null);
        }
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        initToolbar(view);
        initViewPager(view, predefinedPosition);
        initTabs(view);
        return view;
    }

    private void initToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        /* Needed for sync with drawer toolbar */
        DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initViewPager(View view, int predefinedPosition) {
        viewPager = view.findViewById(R.id.view_pager);
        PagerSearchResultAdapter pagerSearchResultAdapter = new PagerSearchResultAdapter(getActivity(), getChildFragmentManager());
        viewPager.setAdapter(pagerSearchResultAdapter);
        viewPager.setOffscreenPageLimit(SearchType.values().length);
        viewPager.setCurrentItem(predefinedPosition);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (searchView != null) {
                    String query = searchView.getQuery().toString();
                    if (!TextUtils.isEmpty(query)) {
                        showSpotifyData(position, query);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void initTabs(View view) {
        TabLayout tabLayout = view.findViewById(R.id.view_pager_tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initSearchView(Menu menu) {
        final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenuItem = menu.findItem(R.id.action_menu_search);
        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager != null ? searchManager.getSearchableInfo(getActivity().getComponentName()) : null);
        searchView.setIconifiedByDefault(predefinedSearchViewQuery != null);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        initOnSearchClickListener();
        initOnQueryTextListener();
        if (predefinedSearchViewQuery != null) {
            searchMenuItem.expandActionView();
            searchView.setQuery(predefinedSearchViewQuery, true);
            searchView.clearFocus();
        }
    }

    private void initOnSearchClickListener() {
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*  Can be overridden for limiting search functionality while there is
                 *  a certain connection state condition (for testing app ConnectionStateRemoteService)
                 *  As example, limit search when device is charging via USB:
                 *  if (onConnectionStateListener.isUsbCharging() != null && onConnectionStateListener.isUsbCharging())
                 *  --> collapseSearchView() and showConnectionStateMessage()
                 */
            }
        });
    }

    private void initOnQueryTextListener() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                showSpotifyData(viewPager.getCurrentItem(), query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                /* Query for text changes only when WiFi connected */
                if (onConnectionStateListener.isWiFiConnected()) {
                    showSpotifyData(viewPager.getCurrentItem(), newText);
                }
                return true;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);
        initSearchView(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void showSpotifyData(int pagerPosition, String query) {
        SearchType searchType = SearchType.getSearchTypeForPagerPosition(pagerPosition);
        if (onConnectionStateListener.isNetworkConnected()) {
            searchType.getNetworkData(query, (this.<Artist, ArtistData>getDataCallback(searchType)));
        } else {
            setFragmentData(searchType.getSavedResults(query));
        }
    }

    @NonNull
    private <T extends RealmObject, E> Callback<E> getDataCallback(final SearchType searchType) {
        return new RetrofitCallback<E>(getActivity()) {
            @Override
            public void onResponse(@NonNull Call<E> call, @NonNull Response<E> response) {
                if (response.isSuccessful() && getActivity() != null && isVisible()) {
                    List<T> data = searchType.getResultFromBody(response.body());
                    setFragmentData(data);
                    searchType.saveResults(data);
                }
            }
        };
    }

    private void setFragmentData(List<? extends RealmObject> details) {
        if (details != null && details.size() > 0) {
            List<Fragment> fragments = getChildFragmentManager().getFragments();
            for (Fragment fragment : fragments) {
                if (fragment instanceof DataProvider) {
                    ((DataProvider) fragment).setData(details);
                }
            }
        } else if (onConnectionStateListener.isNetworkConnected()) {
            Toast.makeText(getActivity(), R.string.msg_no_result_no_network_available, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), R.string.msg_no_result_not_found, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (viewPager != null) {
            outState.putInt(KEY_PAGER_POSITION, viewPager.getCurrentItem());
        }
        if (searchView != null && searchView.getQuery() != null && searchView.getQuery().length() > 0) {
            outState.putString(KEY_SEARCH_VIEW_QUERY, searchView.getQuery().toString());
        }
        super.onSaveInstanceState(outState);
    }

    /*Usage for below methods disabled for testing purposes */

    private void notifyAdapterForImageLoading(boolean enableImageLoading) {
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof DataProvider) {
                ((DataProvider) fragment).enableImageLoading(enableImageLoading);
            }
        }
    }

    private void collapseSearchView(SearchView searchView) {
        searchView.clearFocus();
        searchView.onActionViewCollapsed();
    }

    private void showConnectionStateMessage(int stringResId) {
        Toast.makeText(getContext(), stringResId, Toast.LENGTH_SHORT).show();
    }

}
