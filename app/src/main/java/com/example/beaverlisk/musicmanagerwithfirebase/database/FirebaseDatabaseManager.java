package com.example.beaverlisk.musicmanagerwithfirebase.database;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Favorite;
import com.example.beaverlisk.musicmanagerwithfirebase.model.SpotifyEntity;
import com.example.beaverlisk.musicmanagerwithfirebase.user.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDatabaseManager {

    private static final String CHILD_FAVORITES_PATH = "favorites";
    public static final String CHILD_TYPE_PATH = "type";
    public static final String CHILD_ALBUM_PATH = "album";
    public static final String CHILD_ARTIST_PATH = "artist";
    public static final String CHILD_TRACK_PATH = "track";


    private static FirebaseDatabaseManager instance = null;

    public static FirebaseDatabaseManager getInstance() {
        if (instance == null) {
            instance = new FirebaseDatabaseManager();
        }
        return instance;
    }

    public void saveToDatabase(@NonNull SpotifyEntity spotifyEntity) {
        if (getFavoritesReference() != null) {
            Favorite favorite = new Favorite();
            favorite.setId(spotifyEntity.getId());
            favorite.setName(spotifyEntity.getName());
            favorite.setType(spotifyEntity.getType());
            favorite.setNote(spotifyEntity.getNote());
            getFavoritesReference().child(spotifyEntity.getId()).setValue(favorite);
        }
    }

    public void removeFromDatabase(@NonNull SpotifyEntity spotifyEntity) {
        if (getFavoritesReference() != null) {
            getFavoritesReference().child(spotifyEntity.getId()).removeValue();
        }
    }

    @Nullable
    public FirebaseUser getCurrentUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    @Nullable
    private DatabaseReference getUserReference() {
        FirebaseUser currentUser = getCurrentUser();
        DatabaseReference userReference = null;
        if (currentUser != null) {
            userReference = FirebaseDatabase.getInstance().getReference(currentUser.getUid());
        }
        return userReference;
    }

    @Nullable
    public DatabaseReference getFavoritesReference() {
        FirebaseUser currentUser = getCurrentUser();
        DatabaseReference favoritesReference = null;
        if (currentUser != null) {
            favoritesReference = FirebaseDatabase.getInstance().getReference(currentUser.getUid()).child(CHILD_FAVORITES_PATH);
        }
        return favoritesReference;
    }

    public void saveCurrentUserToDatabase() {
        DatabaseReference userReference = getUserReference();
        FirebaseUser currentUser = getCurrentUser();
        if (userReference != null) {
            User userModel = new User();
            userModel.setUid(currentUser.getUid());
            userModel.setEmail(currentUser.getEmail());
            userModel.setName(currentUser.getDisplayName());
            userReference.setValue(userModel);
        }
    }
}

