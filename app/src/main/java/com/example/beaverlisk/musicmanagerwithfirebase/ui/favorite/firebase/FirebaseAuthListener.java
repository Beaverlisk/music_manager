package com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite.firebase;

public interface FirebaseAuthListener {

    void onAuthSuccess();

    void onAuthFailed();

}
