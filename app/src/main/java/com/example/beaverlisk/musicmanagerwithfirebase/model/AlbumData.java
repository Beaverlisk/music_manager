package com.example.beaverlisk.musicmanagerwithfirebase.model;

import java.util.List;

public class AlbumData {

    private Albums albums;

    public Albums getAlbums() {
        return albums;
    }

    public void setAlbums(Albums albums) {
        this.albums = albums;
    }

    public static class Albums {

        private List<Album> items = null;

        public List<Album> getItems() {
            return items;
        }

        public void setItems(List<Album> items) {
            this.items = items;
        }

    }
}
