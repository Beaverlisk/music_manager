package com.example.beaverlisk.musicmanagerwithfirebase.api;

import com.example.beaverlisk.musicmanagerwithfirebase.model.AlbumData;
import com.example.beaverlisk.musicmanagerwithfirebase.model.ArtistData;
import com.example.beaverlisk.musicmanagerwithfirebase.model.TrackData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface SpotifyRESTClient {

    String SEARCH_ENDPOINT = "v1/search";

    @GET(SEARCH_ENDPOINT)
    Call<ArtistData> getArtistData(@Query("q") String artist, @Query("type") String type);

    @GET(SEARCH_ENDPOINT)
    Call<AlbumData> getAlbumData(@Query("q") String album, @Query("type") String type);

    @GET(SEARCH_ENDPOINT)
    Call<TrackData> getTrackData(@Query("q") String track, @Query("type") String type);

    @GET
    Call<ResponseBody> getImage(@Url String imageUrl);

}
