package com.example.beaverlisk.musicmanagerwithfirebase.ui.search;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.view.StarImageButton;

public class SearchResultViewHolder extends RecyclerView.ViewHolder {

    public TextView tvName;
    public ImageView ivImage;
    public StarImageButton starImageButton;

    public SearchResultViewHolder(View itemView) {
        super(itemView);
        tvName = itemView.findViewById(R.id.tv_item_name);
        ivImage = itemView.findViewById(R.id.iv_item_image);
        starImageButton = itemView.findViewById(R.id.btn_star);
    }
}