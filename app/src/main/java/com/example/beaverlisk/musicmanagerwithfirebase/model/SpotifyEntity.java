package com.example.beaverlisk.musicmanagerwithfirebase.model;

/*Marker Interface for model classes*/
public interface SpotifyEntity {

    String getId();

    String getName();

    String getNote();

    String getType();

}
