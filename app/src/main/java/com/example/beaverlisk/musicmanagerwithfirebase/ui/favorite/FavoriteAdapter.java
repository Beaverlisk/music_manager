package com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.database.FirebaseDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Favorite;
import com.example.beaverlisk.musicmanagerwithfirebase.model.SpotifyEntity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {

    private List<SpotifyEntity> favorites = new ArrayList<>();
    private List<String> keys = new ArrayList<>();
    private OnSpotifyFavoriteItemInteractListener onSpotifyFavoriteItemInteractListener;

    private final ViewGroup progressBarLayout;

    private final DatabaseReference favoritesReference;
    private final ValueEventListener valueEventListener;
    private final ChildEventListener childEventListener;

    FavoriteAdapter(OnSpotifyFavoriteItemInteractListener listener, ViewGroup progressBarLayout) {
        onSpotifyFavoriteItemInteractListener = listener;
        favoritesReference = FirebaseDatabaseManager.getInstance().getFavoritesReference();
        valueEventListener = getValueEventListener();
        childEventListener = getChildEventListener();
        this.progressBarLayout = progressBarLayout;
    }

    private ValueEventListener getValueEventListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBarLayout.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private ChildEventListener getChildEventListener() {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Favorite favorite = dataSnapshot.getValue(Favorite.class);
                String key = dataSnapshot.getKey();
                insertValueToList(favorite, key, previousChildName);
                notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Favorite favorite = dataSnapshot.getValue(Favorite.class);
                String key = dataSnapshot.getKey();
                int index = keys.indexOf(key);
                favorites.set(index, favorite);
                keys.set(index, key);
                notifyItemChanged(index);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                if (!keys.contains(key)) {
                    return;
                }
                int index = keys.indexOf(key);
                favorites.remove(index);
                keys.remove(index);
                notifyItemRemoved(index);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Favorite favorite = dataSnapshot.getValue(Favorite.class);
                String key = dataSnapshot.getKey();
                int index = keys.indexOf(key);
                favorites.remove(index);
                keys.remove(index);
                insertValueToList(favorite, key, previousChildName);
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    void startListeningForEvents() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.addValueEventListener(valueEventListener);
        }
        if (childEventListener != null && favoritesReference != null) {
            favoritesReference.addChildEventListener(childEventListener);
        }
    }

    void stopListeningForEvents() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.removeEventListener(valueEventListener);
        }
        if (childEventListener != null && favoritesReference != null) {
            favoritesReference.removeEventListener(childEventListener);
        }
        favorites.clear();
        keys.clear();
    }

    public class FavoriteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        TextView tvType;
        TextView tvNote;
        ImageButton btnShare;

        FavoriteViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tv_title);
            tvType = view.findViewById(R.id.tv_type);
            tvNote = view.findViewById(R.id.tv_note);
            btnShare = view.findViewById(R.id.btn_share);
            btnShare.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            SpotifyEntity spotifyEntity = favorites.get(getAdapterPosition());
            onSpotifyFavoriteItemInteractListener.onFavoriteItemShareButtonClicked(spotifyEntity);
        }
    }

    @Override
    public FavoriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_item, parent, false);
        return new FavoriteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FavoriteViewHolder holder, int position) {
        SpotifyEntity spotifyEntity = favorites.get(position);
        holder.tvTitle.setText(spotifyEntity.getName());
        holder.tvType.setText(spotifyEntity.getType());
        holder.tvNote.setText(spotifyEntity.getNote());
    }

    @Override
    public int getItemCount() {
        return favorites.size();
    }

    public void setData(@NonNull List<SpotifyEntity> details) {
        this.favorites = details;
        notifyDataSetChanged();
    }

    void removeItem(int position) {
        favorites.remove(position);
        keys.remove(position);
        notifyItemRemoved(position);
    }

    void restoreItem(int position, SpotifyEntity spotifyEntity) {
        favorites.add(position, spotifyEntity);
        /* datasnapshot.getKey() == spotifyEntity.getId() */
        keys.add(position, spotifyEntity.getId());
        notifyItemInserted(position);
    }

    SpotifyEntity getItemByPosition(int position) {
        return favorites.get(position);
    }

    private void insertValueToList(Favorite favorite, String valueKey, String previousChildName) {
        if (previousChildName == null) {
            favorites.add(0, favorite);
            keys.add(0, valueKey);
        } else {
            int previousIndex = keys.indexOf(previousChildName);
            int nextIndex = previousIndex + 1;
            if (nextIndex == favorites.size()) {
                favorites.add(favorite);
                keys.add(valueKey);
            } else {
                favorites.add(nextIndex, favorite);
                keys.add(nextIndex, valueKey);
            }
        }
    }

}
