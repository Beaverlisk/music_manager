package com.example.beaverlisk.musicmanagerwithfirebase.ui.search;

import com.example.beaverlisk.musicmanagerwithfirebase.model.SpotifyEntity;

public interface OnSpotifyListItemListener {

    void onItemFavoriteButtonClicked(SpotifyEntity spotifyEntity, boolean addToFavorite);

}
