package com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification;

import android.app.PendingIntent;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.messaging.RemoteMessage;


public class ConsoleNotification extends BaseNotification {

    public ConsoleNotification(RemoteMessage remoteMessage) {
        super(remoteMessage);
    }

    @Override
    protected PendingIntent configurePendingIntent(Context context) {
        return null;
    }

    @Override
    protected int getLargeIcon() {
        return NotificationConstants.CONSOLE_NOTIFICATION_LARGE_ICON;
    }

    @NonNull
    @Override
    protected String getNotificationTag() {
        return NotificationConstants.CONSOLE_NOTIFICATION_TAG;
    }

    @NonNull
    @Override
    protected int getNotificationID() {
        return NotificationConstants.CONSOLE_NOTIFICATION_ID;
    }

    @Override
    protected String getChannelID() {
        return NotificationConstants.PRIMARY_CHANNEL_ID;
    }

}
