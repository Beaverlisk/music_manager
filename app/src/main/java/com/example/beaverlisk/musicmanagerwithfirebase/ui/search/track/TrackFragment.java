package com.example.beaverlisk.musicmanagerwithfirebase.ui.search.track;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.database.FirebaseDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Track;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.DataProvider;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.OnSpotifyListItemListener;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.view.StarImageButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TrackFragment extends Fragment implements DataProvider {

    private ViewGroup parentLinearLayout;
    private OnSpotifyListItemListener onSpotifyListItemListener;

    List<String> favoritesIds = new ArrayList<>();
    ArrayList<StarImageButton> childViews = new ArrayList<>();
    private DatabaseReference favoritesReference;
    private ValueEventListener valueEventListener;

    public TrackFragment() {
    }

    @NonNull
    public static TrackFragment newInstance() {
        return new TrackFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onSpotifyListItemListener = (OnSpotifyListItemListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() + " must implement OnSpotifyListItemListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track, container, false);
        parentLinearLayout = view.findViewById(R.id.linear_layout_track_fragment);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        favoritesReference = FirebaseDatabaseManager.getInstance().getFavoritesReference();
        valueEventListener = getValueEventListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        startListeningForValueEvent();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopListeningForValueEvent();
    }

    @Override
    public void setData(List<?> data) {
        if (data != null && data.size() > 0 && data.get(0) instanceof Track) {
            List<Track> details = (List<Track>) data;
            parentLinearLayout.removeAllViews();
            for (int i = 0; i < details.size(); i++) {
                final Track track = details.get(i);
                View listItem = getLayoutInflater().inflate(R.layout.list_item_no_image, null);
                TextView textView = listItem.findViewById(R.id.tv_item_name);
                textView.setText(track.getName());
                final StarImageButton starImageButton = listItem.findViewById(R.id.btn_star);
                starImageButton.setTag(track.getId());
                starImageButton.addOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onSpotifyListItemListener.onItemFavoriteButtonClicked(track, starImageButton.isStarred());
                    }
                });
                childViews.add(starImageButton);
                syncFavorites();
                parentLinearLayout.addView(listItem);
            }
        }
    }

    private void syncFavorites() {
        for (StarImageButton button : childViews) {
            if (favoritesIds.contains(button.getTag())) {
                button.toggleStarState(true);
            } else {
                button.toggleStarState(false);
            }
        }
    }

    @Override
    public void enableImageLoading(boolean enable) {
    }

    @Override
    public void onDetach() {
        onSpotifyListItemListener = null;
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        favoritesIds = null;
        childViews = null;
        super.onDestroy();
    }

    void startListeningForValueEvent() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.orderByChild(FirebaseDatabaseManager.CHILD_TYPE_PATH)
                    .equalTo(FirebaseDatabaseManager.CHILD_TRACK_PATH).addValueEventListener(valueEventListener);
        }
    }

    void stopListeningForValueEvent() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.removeEventListener(valueEventListener);
        }
        favoritesIds.clear();
    }

    public ValueEventListener getValueEventListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (favoritesIds != null && favoritesIds.size() > 0) {
                    favoritesIds.clear();
                }
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    String id = childSnapshot.getKey();
                    favoritesIds.add(id);
                }
                syncFavorites();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }
}
