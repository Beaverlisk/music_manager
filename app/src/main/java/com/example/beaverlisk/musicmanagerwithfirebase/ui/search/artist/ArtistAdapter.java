package com.example.beaverlisk.musicmanagerwithfirebase.ui.search.artist;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.image.ImageDownloader;
import com.example.beaverlisk.musicmanagerwithfirebase.database.FirebaseDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Artist;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.OnSpotifyListItemListener;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.SearchResultViewHolder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ArtistAdapter extends BaseAdapter {

    private OnSpotifyListItemListener onSpotifyListItemListener;

    private List<Artist> artistDetails = new ArrayList<>();
    private List<String> favoritesIds = new ArrayList<>();
    private boolean enableImageLoading = true;

    private final DatabaseReference favoritesReference;
    private final ValueEventListener valueEventListener;

    ArtistAdapter(OnSpotifyListItemListener onSpotifyListItemListener) {
        this.onSpotifyListItemListener = onSpotifyListItemListener;
        favoritesReference = FirebaseDatabaseManager.getInstance().getFavoritesReference();
        valueEventListener = getValueEventListener();
    }

    void startListeningForValueEvent() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.orderByChild(FirebaseDatabaseManager.CHILD_TYPE_PATH)
                    .equalTo(FirebaseDatabaseManager.CHILD_ARTIST_PATH).addValueEventListener(valueEventListener);
        }
    }

    void stopListeningForValueEvent() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.removeEventListener(valueEventListener);
        }
        favoritesIds.clear();
        artistDetails.clear();
    }

    @Override
    public int getCount() {
        return artistDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return artistDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Artist artist = artistDetails.get(position);
        final String artistId = artist.getId();
        final SearchResultViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.list_item_with_image, parent, false);
            viewHolder = new SearchResultViewHolder(convertView);
            viewHolder.starImageButton.addOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer position = (Integer) v.getTag();
                    onSpotifyListItemListener.onItemFavoriteButtonClicked(artistDetails.get(position),
                            viewHolder.starImageButton.isStarred());
                }
            });
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SearchResultViewHolder) convertView.getTag();
        }
        viewHolder.tvName.setText(artist.getName());
        viewHolder.starImageButton.setTag(position);
        if (favoritesIds.contains(artistId)) {
            viewHolder.starImageButton.toggleStarState(true);
        } else {
            viewHolder.starImageButton.toggleStarState(false);
        }
        viewHolder.ivImage.setTag(artistId);
        viewHolder.ivImage.setImageResource(R.drawable.ic_default_iv_bg);
        if (artist.getImageUrl() != null && enableImageLoading) {
            ImageDownloader.getInstance().downloadBitmap(viewHolder.ivImage, artistId, artist.getImageUrl());
        }
        return convertView;
    }

    public void setData(@NonNull List<Artist> details) {
        this.artistDetails = details;
        notifyDataSetChanged();
    }

    void enableImageLoading(boolean enableImageLoading) {
        this.enableImageLoading = enableImageLoading;
        notifyDataSetChanged();
    }

    private ValueEventListener getValueEventListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (favoritesIds != null && favoritesIds.size() > 0) {
                    favoritesIds.clear();
                }
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    String id = childSnapshot.getKey();
                    favoritesIds.add(id);
                }
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

}

