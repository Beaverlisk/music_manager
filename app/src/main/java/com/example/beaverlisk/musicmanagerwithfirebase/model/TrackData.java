package com.example.beaverlisk.musicmanagerwithfirebase.model;

import java.util.List;

public class TrackData {

    private Tracks tracks;

    public Tracks getTracks() {
        return tracks;
    }

    public void setTracks(Tracks tracks) {
        this.tracks = tracks;
    }

    public static class Tracks {

        private List<Track> items = null;

        public List<Track> getItems() {
            return items;
        }

        public void setItems(List<Track> items) {
            this.items = items;
        }
    }
}