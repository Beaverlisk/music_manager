package com.example.beaverlisk.musicmanagerwithfirebase.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;

public class NetworkUtil {

    public static boolean isNetworkConnected(Context appContext) {
        return getActiveNetworkInfo(appContext) != null && getActiveNetworkInfo(appContext).isConnectedOrConnecting();
    }

    public static boolean isWifiConnected(Context appContext) {
        NetworkInfo networkInfo = getActiveNetworkInfo(appContext);
        return networkInfo != null && networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static boolean isMobileDataConnected(Context appContext) {
        NetworkInfo networkInfo = getActiveNetworkInfo(appContext);
        return networkInfo != null && networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
    }

    @Nullable
    private static NetworkInfo getActiveNetworkInfo(Context appContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo();
    }
}
