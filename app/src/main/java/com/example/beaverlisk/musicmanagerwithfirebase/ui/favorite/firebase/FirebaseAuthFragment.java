package com.example.beaverlisk.musicmanagerwithfirebase.ui.favorite.firebase;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import org.jetbrains.annotations.Contract;

public class FirebaseAuthFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = FirebaseAuthFragment.class.getSimpleName() + "_Tag";
    private static final int REQUEST_CODE_GOOGLE_SIGN_IN = 9001;

    private FirebaseAuth firebaseAuth;
    private GoogleSignInClient googleSignInClient;
    private FirebaseAuthListener firebaseAuthListener;

    private EditText editTextEmail;
    private EditText editTextPassword;

    public FirebaseAuthFragment() {
    }

    public static FirebaseAuthFragment newInstance() {
        return new FirebaseAuthFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            firebaseAuthListener = (FirebaseAuthListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() + " must implement FirebaseAuthListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
        googleSignInClient = GoogleSignIn.getClient(getActivity(), getGoogleSignInOptions());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_firebase_auth, container, false);
        editTextEmail = view.findViewById(R.id.edit_text_email);
        editTextPassword = view.findViewById(R.id.edit_text_password);
        initToolbar(view);
        initButtons(view);
        return view;
    }

    private void initToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.fragment_firebase_auth_toolbar_title);
        /* Needed for sync with drawer toolbar */
        DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initButtons(View view) {
        Button buttonLogin = view.findViewById(R.id.btn_login);
        Button buttonRegister = view.findViewById(R.id.btn_register);
        Button buttonLoginWithGoogle = view.findViewById(R.id.btn_login_with_google);
        buttonLogin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);
        buttonLoginWithGoogle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();
        switch (v.getId()) {
            case R.id.btn_login:
                singInWithEmail(email, password);
                break;
            case R.id.btn_register:
                createUser(email, password);
                break;
            case R.id.btn_login_with_google:
                singInWithGoogle();
                break;
        }
    }

    private void createUser(@Nullable String email, @Nullable String password) {
        if (!validateForm(email, password)) {
            return;
        }
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(), getAuthResult());
    }

    private void singInWithEmail(@Nullable String email, @Nullable String password) {
        if (!validateForm(email, password)) {
            return;
        }
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(), getAuthResult());
    }

    private void singInWithGoogle() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE_SIGN_IN);
    }

    private GoogleSignInOptions getGoogleSignInOptions() {
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Toast.makeText(getActivity(), R.string.msg_authentication_failed, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(getActivity(), getAuthResult());
    }

    private OnCompleteListener<AuthResult> getAuthResult() {
        return new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (firebaseAuthListener != null) {
                    if (task.isSuccessful()) {
                        firebaseAuthListener.onAuthSuccess();
                    } else {
                        firebaseAuthListener.onAuthFailed();
                    }
                }
            }
        };
    }


    @Contract("null, _ -> false; _, null -> false")
    private boolean validateForm(@Nullable String email, @Nullable String password) {
        boolean valid = true;

        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getString(R.string.msg_required));
            valid = false;
        } else {
            editTextEmail.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            editTextPassword.setError(getString(R.string.msg_required));
            valid = false;
        } else {
            editTextPassword.setError(null);
        }
        return valid;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        firebaseAuthListener = null;
    }
}

