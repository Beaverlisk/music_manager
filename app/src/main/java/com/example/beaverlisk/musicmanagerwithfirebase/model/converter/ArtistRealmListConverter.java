package com.example.beaverlisk.musicmanagerwithfirebase.model.converter;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Artist;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import io.realm.RealmList;

public class ArtistRealmListConverter implements JsonSerializer<RealmList<Artist>>, JsonDeserializer<RealmList<Artist>> {

    @Override
    public JsonElement serialize(RealmList<Artist> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (Artist artist : src) {
            jsonArray.add(context.serialize(artist));
        }
        return jsonArray;
    }

    @Override
    public RealmList<Artist> deserialize(JsonElement json, Type typeOfT,
                                         JsonDeserializationContext context) throws JsonParseException {
        RealmList<Artist> artists = new RealmList<>();
        JsonArray jsonArray = json.getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            artists.add((Artist) context.deserialize(jsonElement, Artist.class));
        }
        return artists;
    }
}
