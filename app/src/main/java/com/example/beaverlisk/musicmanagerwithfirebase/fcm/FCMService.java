package com.example.beaverlisk.musicmanagerwithfirebase.fcm;

import com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification.NotificationCreator;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FCMService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // TODO: implement different handling for data and notification message types
        if (remoteMessage.getData().size() > 0 || remoteMessage.getNotification() != null) {
            showNotification(remoteMessage);
        }
    }

    private void showNotification(RemoteMessage remoteMessage) {
        new NotificationCreator(getApplicationContext()).showNotification(remoteMessage);
    }
}
