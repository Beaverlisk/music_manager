package com.example.beaverlisk.musicmanagerwithfirebase.model.converter;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Album;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import io.realm.RealmList;

public class AlbumRealmListConverter implements JsonSerializer<RealmList<Album>>, JsonDeserializer<RealmList<Album>> {

    @Override
    public JsonElement serialize(RealmList<Album> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (Album album : src) {
            jsonArray.add(context.serialize(album));
        }
        return jsonArray;
    }

    @Override
    public RealmList<Album> deserialize(JsonElement json, Type typeOfT,
                                        JsonDeserializationContext context) throws JsonParseException {
        RealmList<Album> albums = new RealmList<>();
        JsonArray jsonArray = json.getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            albums.add((Album) context.deserialize(jsonElement, Album.class));
        }
        return albums;
    }
}
