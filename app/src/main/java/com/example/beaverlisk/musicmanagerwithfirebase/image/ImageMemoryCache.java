package com.example.beaverlisk.musicmanagerwithfirebase.image;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.LruCache;

class ImageMemoryCache {

    private final LruCache<String, Bitmap> memoryCache;

    ImageMemoryCache() {
        memoryCache = initializeCache();
    }

    @NonNull
    private LruCache<String, Bitmap> initializeCache() {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        int cacheSize = maxMemory / 8;
        return new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String imageUrl, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    Bitmap getBitmapFromMemoryCache(String key) {
        return memoryCache.get(key);
    }

    void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemoryCache(key) == null) {
            memoryCache.put(key, bitmap);
        }
    }

    void clearMemoryCache() {
        memoryCache.evictAll();
    }

}
