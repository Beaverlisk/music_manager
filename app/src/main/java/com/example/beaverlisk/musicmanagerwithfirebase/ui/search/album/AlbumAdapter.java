package com.example.beaverlisk.musicmanagerwithfirebase.ui.search.album;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.beaverlisk.musicmanagerwithfirebase.R;
import com.example.beaverlisk.musicmanagerwithfirebase.image.ImageDownloader;
import com.example.beaverlisk.musicmanagerwithfirebase.database.FirebaseDatabaseManager;
import com.example.beaverlisk.musicmanagerwithfirebase.model.Album;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.OnSpotifyListItemListener;
import com.example.beaverlisk.musicmanagerwithfirebase.ui.search.SearchResultViewHolder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AlbumAdapter extends RecyclerView.Adapter<SearchResultViewHolder> {

    private List<Album> albumDetails = new ArrayList<>();
    private List<String> favoritesIds = new ArrayList<>();
    private boolean enableImageLoading = true;
    private OnSpotifyListItemListener onSpotifyListItemListener;

    private final DatabaseReference favoritesReference;
    private final ValueEventListener valueEventListener;

    AlbumAdapter(OnSpotifyListItemListener onSpotifyListItemListener) {
        this.onSpotifyListItemListener = onSpotifyListItemListener;
        favoritesReference = FirebaseDatabaseManager.getInstance().getFavoritesReference();
        valueEventListener = getValueEventListener();
    }

    void startListeningForValueEvent() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.orderByChild(FirebaseDatabaseManager.CHILD_TYPE_PATH).equalTo(FirebaseDatabaseManager.CHILD_ALBUM_PATH).addValueEventListener(valueEventListener);
        }
    }

    void stopListeningForValueEvent() {
        if (valueEventListener != null && favoritesReference != null) {
            favoritesReference.removeEventListener(valueEventListener);
        }
        albumDetails.clear();
        favoritesIds.clear();
    }

    @Override
    public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_with_image, parent, false);
        final SearchResultViewHolder searchResultViewHolder = new SearchResultViewHolder(view);
        searchResultViewHolder.starImageButton.addOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Album album = albumDetails.get(searchResultViewHolder.getAdapterPosition());
                onSpotifyListItemListener.onItemFavoriteButtonClicked(album, searchResultViewHolder.starImageButton.isStarred());
            }
        });
        return searchResultViewHolder;
    }

    @Override
    public void onBindViewHolder(final SearchResultViewHolder viewHolder, int position) {
        final Album album = albumDetails.get(position);
        String albumId = album.getId();
        viewHolder.tvName.setText(album.getName());
        viewHolder.ivImage.setTag(albumId);
        viewHolder.ivImage.setImageResource(R.drawable.ic_default_iv_bg);
        if (favoritesIds.contains(albumId)) {
            viewHolder.starImageButton.toggleStarState(true);
        } else {
            viewHolder.starImageButton.toggleStarState(false);
        }
        if (album.getImageUrl() != null && enableImageLoading) {
            ImageDownloader.getInstance().downloadBitmap(viewHolder.ivImage, albumId, album.getImageUrl());
        }
    }

    @Override
    public int getItemCount() {
        return albumDetails.size();
    }

    public void setData(@NonNull List<Album> details) {
        this.albumDetails = details;
        notifyDataSetChanged();
    }

    void enableImageLoading(boolean enableImageLoading) {
        this.enableImageLoading = enableImageLoading;
        notifyDataSetChanged();
    }

    private ValueEventListener getValueEventListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (favoritesIds != null && favoritesIds.size() > 0) {
                    favoritesIds.clear();
                }
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    String id = childSnapshot.getKey();
                    favoritesIds.add(id);
                }
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

}
