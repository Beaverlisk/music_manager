package com.example.beaverlisk.musicmanagerwithfirebase;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;

import com.crashlytics.android.Crashlytics;
import com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification.NotificationConstants;
import com.example.beaverlisk.musicmanagerwithfirebase.fcm.notification.NotificationHelper;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;


public class MusicManagerApplication extends Application {

    private static final String REALM_FILENAME = "mm_search_result.realm";
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        MusicManagerApplication.appContext = getApplicationContext();
        initRealm();
        initFabric();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createPrimaryNotificationChannel();
        }
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(REALM_FILENAME)
                .build();
        Realm.setDefaultConfiguration(config);
    }

    private void initFabric() {
        Fabric.with(this, new Crashlytics());
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createPrimaryNotificationChannel() {
        new NotificationHelper(this).createNotificationChannel(NotificationConstants.PRIMARY_CHANNEL_ID);
    }

    /* Static reference to app context to be used in singleton classes*/
    public static Context getAppContext() {
        return MusicManagerApplication.appContext;
    }

}
