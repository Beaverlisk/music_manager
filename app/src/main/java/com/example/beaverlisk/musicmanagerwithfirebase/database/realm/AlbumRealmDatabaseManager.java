package com.example.beaverlisk.musicmanagerwithfirebase.database.realm;


import android.support.annotation.NonNull;

import com.example.beaverlisk.musicmanagerwithfirebase.model.Album;

import java.util.List;

public class AlbumRealmDatabaseManager extends RealmDatabaseManager<Album> {

    private static AlbumRealmDatabaseManager instance = null;

    private AlbumRealmDatabaseManager() {
        super(Album.class);
    }

    public static AlbumRealmDatabaseManager getInstance() {
        if (instance == null) {
            instance = new AlbumRealmDatabaseManager();
        }
        return instance;
    }

    public void saveToDatabase(List<Album> data) {
        super.saveToDatabase(data);
    }

    @NonNull
    public List<Album> qetRealmObjectsMatchingQuery(String query) {
        return super.qetRealmObjectsMatchingQuery(query);
    }
}


