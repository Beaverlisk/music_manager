package com.example.musicmanagerwidget;

import android.app.DownloadManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.RemoteViews;
import android.widget.Toast;

public class MusicManagerWidgetProvider extends AppWidgetProvider {

    public static final String KEY_CATEGORY = "category";
    public static final String KEY_SHARE_STRING = "shareString";
    public static final String CATEGORY_ALL = "all";
    public static final String CATEGORY_ARTIST = "artist";
    public static final String CATEGORY_ALBUM = "album";
    public static final String CATEGORY_TRACK = "track";

    /* onEnabled is called when an instance the App Widget is created for the first time. For example,
     * if the user adds two instances of your App Widget, this is only called the first time.
     * If you need to open a new database or perform other setup that only needs to occur once
     * for all App Widget instances, then this is a good place to do it.
     */
    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    /* onUpdate is called to update the App Widget at intervals defined by the updatePeriodMillis
     * attribute and when the user adds the App Widget, so it should perform the essential setup,
     * such as define event handlers for Views and start a temporary Service, if necessary.
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int widgetId : appWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            setWidgetIntents(context, remoteViews, widgetId);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private void setWidgetIntents(Context context, RemoteViews remoteViews, int widgetId) {
        setServiceIntent(context, remoteViews, widgetId, R.id.flipper);
        setNavigationIntent(context, remoteViews, WidgetAction.NEXT_ITEM_ACTION.getActionString(), widgetId, R.id.btn_next);
        setNavigationIntent(context, remoteViews, WidgetAction.PREVIOUS_ITEM_ACTION.getActionString(), widgetId, R.id.btn_previous);
        setCategoryIntent(context, remoteViews, WidgetAction.CATEGORY_ALL_DATA_ACTION.getActionString(), widgetId, R.id.btn_all);
        setCategoryIntent(context, remoteViews, WidgetAction.CATEGORY_ARTIST_ACTION.getActionString(), widgetId, R.id.btn_artist);
        setCategoryIntent(context, remoteViews, WidgetAction.CATEGORY_ALBUM_ACTION.getActionString(), widgetId, R.id.btn_album);
        setCategoryIntent(context, remoteViews, WidgetAction.CATEGORY_TRACK_ACTION.getActionString(), widgetId, R.id.btn_track);
        setItemTouchIntent(context, remoteViews, WidgetAction.ITEM_TOUCH_ACTION.getActionString(), widgetId, R.id.flipper);
    }

    private void setServiceIntent(Context context, RemoteViews remoteViews, int widgetId, int adapterViewId) {
        Intent serviceIntent = new Intent(context, FlipperWidgetService.class);
        serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        serviceIntent.setData(getIntentScheme(serviceIntent));
        remoteViews.setRemoteAdapter(adapterViewId, serviceIntent);
    }

    private void setNavigationIntent(Context context, RemoteViews remoteViews, String action, int widgetId, int viewId) {
        PendingIntent pendingIntent = getPendingIntent(context, action, widgetId, false);
        remoteViews.setOnClickPendingIntent(viewId, pendingIntent);
    }

    private void setCategoryIntent(Context context, RemoteViews remoteViews, String action, int widgetId, int viewId) {
        PendingIntent pendingIntent = getPendingIntent(context, action, widgetId, true);
        remoteViews.setOnClickPendingIntent(viewId, pendingIntent);
    }

    private void setItemTouchIntent(Context context, RemoteViews remoteViews, String action, int widgetId, int adapterViewId) {
        PendingIntent pendingIntent = getPendingIntent(context, action, widgetId, true);
        remoteViews.setPendingIntentTemplate(adapterViewId, pendingIntent);
    }

    private PendingIntent getPendingIntent(Context context, String action, int widgetId, boolean withScheme) {
        Intent internalIntent = new Intent(context, MusicManagerWidgetProvider.class);
        internalIntent.setAction(action);
        internalIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        if (withScheme) {
            internalIntent.setData(getIntentScheme(internalIntent));
        }
        return PendingIntent.getBroadcast(context, widgetId, internalIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private Uri getIntentScheme(Intent intent) {
        return Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        int widgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        WidgetAction widgetAction = WidgetAction.getAction(intent.getAction());
        String categoryActionForServiceUpdate = null;
        switch (widgetAction) {
            case NEXT_ITEM_ACTION:
                remoteViews.showPrevious(R.id.flipper);
                break;
            case PREVIOUS_ITEM_ACTION:
                remoteViews.showNext(R.id.flipper);
                break;
            case CATEGORY_ALL_DATA_ACTION:
                categoryActionForServiceUpdate = WidgetAction.CATEGORY_ALL_DATA_ACTION.getActionString();
                break;
            case CATEGORY_ARTIST_ACTION:
                categoryActionForServiceUpdate = WidgetAction.CATEGORY_ARTIST_ACTION.getActionString();
                break;
            case CATEGORY_ALBUM_ACTION:
                categoryActionForServiceUpdate = WidgetAction.CATEGORY_ALBUM_ACTION.getActionString();
                break;
            case CATEGORY_TRACK_ACTION:
                categoryActionForServiceUpdate = WidgetAction.CATEGORY_TRACK_ACTION.getActionString();
                break;
            case ITEM_TOUCH_ACTION:
                String selectedCategory = intent.getStringExtra(KEY_CATEGORY);
                Intent externalIntent = null;
                switch (selectedCategory) {
                    case CATEGORY_ALL:
                        String shareString = intent.getStringExtra(KEY_SHARE_STRING);
                        if (shareString != null) {
                            externalIntent = setShareIntent(shareString);
                        } else {
                            Toast.makeText(context, "Nothing to share", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case CATEGORY_ARTIST:
                        externalIntent = setExternalIntent(WidgetAction.OPEN_MAIN_APP_ACTION.getActionString());
                        break;
                    case CATEGORY_ALBUM:
                        externalIntent = setExternalIntent(DownloadManager.ACTION_VIEW_DOWNLOADS);
                        break;
                    case CATEGORY_TRACK:
                        externalIntent = setExternalIntent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);
                        break;
                }
                if (externalIntent != null && externalIntent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(setChooser(context, externalIntent));
                }
                break;
        }

        if (categoryActionForServiceUpdate != null) {
            notifyRemoteViewsServiceForDataChange(context, categoryActionForServiceUpdate, widgetId);
            AppWidgetManager.getInstance(context).notifyAppWidgetViewDataChanged(widgetId, R.id.flipper);
        }

        /* There is a problem with AppWidgetManager.partiallyUpdateAppWidget() method
         * related to this open issue: https://issuetracker.google.com/issues/37136552
         */
        AppWidgetManager.getInstance(context).updateAppWidget(widgetId, remoteViews);
        super.onReceive(context, intent);
    }

    private Intent setShareIntent(String shareString) {
        Intent shareIntent = setExternalIntent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareString);
        shareIntent.setType("text/plain");
        return shareIntent;
    }

    private Intent setExternalIntent(String action) {
        Intent externalIntent = new Intent(action);
        externalIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return externalIntent;
    }

    private Intent setChooser(Context context, Intent externalIntent) {
        Intent chooserIntent = Intent.createChooser(externalIntent, context.getString(R.string.chooser_title_open_with));
        chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return chooserIntent;
    }

    public void notifyRemoteViewsServiceForDataChange(Context context, String action, int widgetId) {
        Intent serviceNotifyIntent = new Intent(context, FlipperWidgetService.FlipperRemoteViewsFactory.class);
        serviceNotifyIntent.setAction(action);
        serviceNotifyIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(serviceNotifyIntent);
    }
}

