package com.example.musicmanagerwidget;

public enum WidgetAction {

    INVALID_ACTION("com.example.musicmanagerwidget.action.invalid"),
    NEXT_ITEM_ACTION("com.example.musicmanagerwidget.action.nextItem"),
    PREVIOUS_ITEM_ACTION("com.example.musicmanagerwidget.action.previousItem"),
    CATEGORY_ALL_DATA_ACTION("com.example.musicmanagerwidget.action.categoryAllData"),
    CATEGORY_ARTIST_ACTION("com.example.musicmanagerwidget.action.categoryArtist"),
    CATEGORY_ALBUM_ACTION("com.example.musicmanagerwidget.action.categoryAlbum"),
    CATEGORY_TRACK_ACTION("com.example.musicmanagerwidget.action.categoryTrack"),
    ITEM_TOUCH_ACTION("com.example.musicmanagerwidget.action.itemTouch"),
    OPEN_MAIN_APP_ACTION("com.example.beaverlisk.musicmanager.action.openMusicManagerApp");

    private String action;

    WidgetAction(String action) {
        this.action = action;
    }

    public String getActionString() {
        return action;
    }

    public static WidgetAction getAction(String action) {
        for (WidgetAction widgetAction : values()) {
            if (widgetAction.getActionString().equals(action)) {
                return widgetAction;
            }
        }
        return INVALID_ACTION;
    }
}
