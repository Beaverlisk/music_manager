package com.example.musicmanagerwidget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;
import java.util.List;

public class FlipperWidgetService extends RemoteViewsService {

    private static final String URI_STRING_ARTIST = "content://com.example.beaverlisk.musicmanager.provider.Favorite/Artist";
    private static final String URI_STRING_ALBUM = "content://com.example.beaverlisk.musicmanager.provider.Favorite/Album";
    private static final String URI_STRING_TRACK = "content://com.example.beaverlisk.musicmanager.provider.Favorite/Track";
    private static final String URI_STRING_ALL_DATA = "content://com.example.beaverlisk.musicmanager.provider.Favorite/All";
    private static final String DB_COLUMN_NAME = "name";
    private static final String DB_COLUMN_NOTE = "note";

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new FlipperRemoteViewsFactory(intent);
    }

    class FlipperRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

        private BroadcastReceiver widgetCategoryBroadcastReceiver;
        private List<WidgetItem> dataList;
        private Uri uriArtist, uriAlbum, uriTrack, uriAllData;
        private String action = WidgetAction.CATEGORY_ALL_DATA_ACTION.getActionString();
        String selectedCategory = null;

        FlipperRemoteViewsFactory(Intent intent) {
            dataList = new ArrayList<>();
            uriArtist = Uri.parse(URI_STRING_ARTIST);
            uriAlbum = Uri.parse(URI_STRING_ALBUM);
            uriTrack = Uri.parse(URI_STRING_TRACK);
            uriAllData = Uri.parse(URI_STRING_ALL_DATA);
        }

        @Override
        public void onCreate() {
            widgetCategoryBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    action = intent.getAction();

                }
            };
            LocalBroadcastManager.getInstance(getApplicationContext())
                    .registerReceiver(widgetCategoryBroadcastReceiver, getCategoryIntentFilter());
        }

        @NonNull
        private IntentFilter getCategoryIntentFilter() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WidgetAction.CATEGORY_ALL_DATA_ACTION.getActionString());
            intentFilter.addAction(WidgetAction.CATEGORY_ARTIST_ACTION.getActionString());
            intentFilter.addAction(WidgetAction.CATEGORY_ALBUM_ACTION.getActionString());
            intentFilter.addAction(WidgetAction.CATEGORY_TRACK_ACTION.getActionString());
            return intentFilter;
        }

        @Override
        public void onDataSetChanged() {
            if (dataList != null && dataList.size() > 0) {
                dataList.clear();
            }
            WidgetAction widgetAction = WidgetAction.getAction(action);
            switch (widgetAction) {
                case CATEGORY_ALL_DATA_ACTION:
                    queryAllData();
                    selectedCategory = MusicManagerWidgetProvider.CATEGORY_ALL;
                    break;
                case CATEGORY_ARTIST_ACTION:
                    queryArtist();
                    selectedCategory = MusicManagerWidgetProvider.CATEGORY_ARTIST;
                    break;
                case CATEGORY_ALBUM_ACTION:
                    queryAlbum();
                    selectedCategory = MusicManagerWidgetProvider.CATEGORY_ALBUM;
                    break;
                case CATEGORY_TRACK_ACTION:
                    queryTrack();
                    selectedCategory = MusicManagerWidgetProvider.CATEGORY_TRACK;
                    break;
                default:
                    break;
            }
        }

        private void queryArtist() {
            Cursor cursor = getApplicationContext().getContentResolver()
                    .query(uriArtist, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        WidgetItem widgetItem = new WidgetItem();
                        widgetItem.setName(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NAME)));
                        widgetItem.setType(getString(R.string.artist_type_title));
                        widgetItem.setNote(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NOTE)));
                        dataList.add(widgetItem);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        private void queryAlbum() {
            Cursor cursor = getApplicationContext().getContentResolver()
                    .query(uriAlbum, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        WidgetItem widgetItem = new WidgetItem();
                        widgetItem.setName(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NAME)));
                        widgetItem.setType(getString(R.string.album_type_title));
                        widgetItem.setNote(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NOTE)));
                        dataList.add(widgetItem);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        private void queryTrack() {
            Cursor cursor = getApplicationContext().getContentResolver().query(uriTrack, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        WidgetItem widgetItem = new WidgetItem();
                        widgetItem.setName(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NAME)));
                        widgetItem.setType(getString(R.string.track_type_title));
                        widgetItem.setNote(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NOTE)));
                        dataList.add(widgetItem);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        private void queryAllData() {
            Cursor cursor = getApplicationContext().getContentResolver().query(uriAllData, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        WidgetItem entity = new WidgetItem();
                        entity.setName(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NAME)));
                        entity.setType(getString(R.string.all_data_type_title));
                        entity.setNote(cursor.getString(cursor.getColumnIndex(DB_COLUMN_NOTE)));
                        dataList.add(entity);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        @Override
        public RemoteViews getViewAt(int position) {
            RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.widget_item);
            if (dataList != null && dataList.size() > 0) {
                WidgetItem widgetItem = dataList.get(position);
                remoteViews.setTextViewText(R.id.tv_title, widgetItem.getName());
                remoteViews.setTextViewText(R.id.tv_type, widgetItem.getType());
                remoteViews.setTextViewText(R.id.tv_note, widgetItem.getNote());
                remoteViews.setOnClickFillInIntent(R.id.widget_item_row, setFillIntent(position));
            } else {
                remoteViews.setTextViewText(R.id.tv_type, getString(R.string.msg_nothing_to_show));
            }
            return remoteViews;
        }

        private Intent setFillIntent(int position) {
            WidgetAction categoryAction = WidgetAction.getAction(action);
            Intent fillInIntent = new Intent();
            Bundle intentExtras = new Bundle();
            if (selectedCategory != null) {
                intentExtras.putString(MusicManagerWidgetProvider.KEY_CATEGORY, selectedCategory);
            }
            if (categoryAction == WidgetAction.CATEGORY_ALL_DATA_ACTION) {
                String shareString = getString(R.string.favorite_to_share, dataList.get(position).getName());
                intentExtras.putString(MusicManagerWidgetProvider.KEY_SHARE_STRING, shareString);
            }
            fillInIntent.putExtras(intentExtras);
            return fillInIntent;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public void onDestroy() {
            selectedCategory = null;
            if (dataList != null) {
                dataList.clear();
            }
            if (widgetCategoryBroadcastReceiver != null) {
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(widgetCategoryBroadcastReceiver);
            }
        }
    }

}
