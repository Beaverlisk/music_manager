package com.example.musicmanagerwidget;


public class WidgetItem {

    private String name;
    private String type;
    private String note;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String getType() {
        return type;
    }

    void setType(String type) {
        this.type = type;
    }

    String getNote() {
        return note;
    }

    void setNote(String note) {
        this.note = note;
    }

}
